const messages = {
  // ANGLAIS
  en: {
    message: {
      hello: 'hello world',
      error: 'Error',
      email: 'Email',
      password: 'Password',
      login: 'Login',
      logout: 'Logout',
      createaccount: 'Create an account',
      forgotPassword: 'Forgot your password',
      name: 'Name',
      repassword: 'Re-password',
      postcode: 'Postcode',
      Send: 'Send',
      signin: 'Sign up',
      alreadyCreatedAccount: 'Already have an account',
      home: 'Home',
      ChoiceofPole: 'Choice of pole',
      ChoiceofProject: 'Choice of project',
      Pending: 'Pending',
      invitedtoBeAdmin: 'invited you to become an administrator',
      invitedYou: 'invited you',
      actionsToBeDoneMoreThan: 'Actions still to be done which have more than:',
      actionsToBeDoneNoContributorMoreThan: 'Actions still to be done and without a contributor that have more than:',
      Actions: 'Actions',
      Member: 'Member',
      MemberValidDescription: 'You must be a validated member by an administrator of the organization',
      ChoiceOfPole: 'Choice of pole',
      ChoiceofPoleDescription: 'On this page you will have to choose among the different poles of your organization in order to see the events and the actions which are part of it. If you want to see the events by date, please go to "agenda"',
      ChoiceOfProjects: 'Choice of projects',
      ChoiceOfProjectsDescription: 'Choice of projects description',
      Dashboard: 'Dashboard',
      DashboardUserConfig: 'Dashboard user configuration',
      allfields: 'Not completed all fields',
      nameTooShort: 'Name is too short',
      userNameTooShort: 'Username is too short',
      userNameIncorrect: "The username is incorrect: Only the characters A-Z, a-z, 0-9 and '-' are accepted.",
      passwordTooShort: 'Password is too short',
      emailNotValid: 'Your account is not validated : please check your mailbox to validate your email address.',
      notSamePassword: 'Not the same password',
      memberActions: 'Member actions',
      withContributors: 'with contributors :',
      withoutContributors: 'without contributors:',
      whereIamContributors: 'chere I am contributors :',
      byTags: 'by tags :',
      byMilestone: 'by milestone :',
      byProjects: 'by projects :',
      veryOld: 'very old :',
      descendingSort: 'descending sort',
      Sort: 'Sort',
      sortProjectsBy: 'Sort projects by :',
      sortEventsBy: 'Sort events by :',
      sortActionsBy: 'Sort actions by :',
      numberMonth: '__count__ month',
      filterByTags: 'Filter by tags',
      filterByMilestone: 'Filter by milestone',
      filterByUser: 'Filter by user',
      filterActions: 'Filter actions',
      searchActionPlaceholder: 'Search for an action by name, #tag, :project...',
      searchAnswerPlaceholder: 'Search for a proposal by name, #tag, !status...',
      OrganizationOCECO: 'Organization OCECO',
      ActivateOrganizationOceco: 'Activate an organization in oceco',
      Notifications: 'Notifications',
      ChangeOrganization: 'Change organization',
      TimeSpace: 'Time space',
      NoLongerMemberOrga: 'No longer be a member of the organization',
      BecomeMemberOrga: 'Become a member of the organization',
      OrganizationActions: 'Oranization actions',
      Canceled: 'Canceled',
      Finished: 'Finished',
      You_will_earn_count_credit: 'You will earn <strong>__count__</strong> credit',
      You_will_earn_count_credit_plural: 'You will earn <strong>__count__</strong> credits',
      You_will_be_deducted_from_count_credit: 'You will be deducted from __count__ credit',
      You_will_be_deducted_from_count_credit_plural: 'You will be deducted from __count__ credits',
      Start_on_day: 'Start on __day__',
      Duration_in_hours: 'Duration __hours__ H',
      number_of_participant: 'There are __count__ participants',
      number_of_participant_plural: 'There are __count__ participants',
      minimum_participant: 'For a minimum need of __count__ participants',
      minimum_participant_plural: 'For a minimum need of __count__ participants',
      maximum_participant: 'And a maximum of __count__ participants',
      maximum_participant_plural: 'And a maximum of __count__ participants',
      count_commentaire: '__count__ comment',
      count_commentaire_plural: '__count__ comments',
      duration: 'Duration',
      Project: 'Project',
      start: 'start',
      end: 'end',
      distance: 'Distance',
      Hide_action_count: 'Hide __count__ action',
      Hide_action_count_plural: 'Hide __count__ actions',
      See_action_count: 'See __count__ action',
      See_action_count_plural: 'See __count__ actions',
      EventActions: 'Event actions',
      begin: 'begin',
      Credits: 'Credits',
      StartactionNow: ' Start the action now',
      AlreadyComplete: 'Already complete',
      Spent_count: 'Spent __count__',
      InProgress: 'In progress',
      ProjectActions: 'Project actions',
      ProjectEvents: 'Project events',
      NoUpcomingEvent: 'No upcoming event',
      YourTimeCredit: 'Your time credit',
      ShareUrl: 'Share url',
      Use_the_copy_function_when_you_click_on_the_field_where_there_is_the_link: 'Use the copy function when you click on the field where there is the link',
      Copy: 'Copy',
      url: 'url',
      share: 'Share',
      textTitre: 'Text : Title',
      textInfo: 'Text : Info',
      textBouton: 'Text : Button',
      goBack: 'Go back',
      EditCitoyen: 'Edit a citoyen',
      edit: 'edit',

    },
    sortModal: {
      name: 'name',
      activity_date: 'activity date',
      start_date: 'start date',
      end_date: 'end date',
      creation_date: 'creation date',
      contributor: 'contributor',
      comment: 'comment',
      credits: 'credits',
    },
    dda: {
      resolution: {
        resolution: 'Resolution',
        adopted: "The <strong>resolution</strong> was taken: the proposal is <span class='balanced'>validated</span>",
        refused: "The <strong>resolution</strong> was denied: the proposal is <span class='assertive'>refused</span>",
        endofvote: 'End of vote',
        showproposaldetail: 'Show proposal detail',
        showresolution: 'Show Resolution',
        resultstitle: 'Results',
      },
      actions: {
        action: 'Action',
        actionstartenddate: 'Action from __start__ to __end__',
        spentstartenddate: 'Spent from __start__ to __end__',
        endofaction: 'End of the action',
        theytakeaction: 'They take part in this action',
        youparticipatingaction: 'You are participating in this action',
        wantparticipateaction: 'I want to participate in this action',
        noparticipants: 'No participants',
        discussion: 'Discussion',
        buttons: {
          disable: 'Disable',
          close: 'Close',
        },
        status: {
          inprogress: 'In progress',
          completed: 'Completed',
          todo: 'To do',
          disabled: 'Disabled',
        },
      },
      proposal: {
        proposal: 'Proposal',
        argumentstitle: 'Supplemental information, arguments, examples, demonstrations, etc.',
        amendments: {
          title: 'Amendments',
          text: 'Proposal submitted to amendments until',
          endofamendments: 'End of amendments',
          proposetext: 'You can propose amendments and vote on amendments proposed by other users',
          listofamendmentstemporarily: 'List of amendments temporarily validated · Majority: 50%',
          amendmentsdeactivated: 'Amendments deactivated',
          noamendments: 'No amendments',
          validated: 'validated',
          validated_temporarily: 'temporarily validated',
          refused: 'refused',
          refused_temporarily: 'temporarily refused',
        },
        tovote: {
          title: 'Vote',
          text: 'Proposal to be put to the vote until',
          endofvotes: 'End of votes',
          proposetext: 'You can propose amendments and vote on amendments proposed by other users',
          listofamendmentsvalidated: 'List of validated amendments · Majority',
          votesdisabled: 'votes disabled',
          proposal_temporarily_validated: "Proposal temporarily <span class='balanced'>validated</span>",
          proposal_temporarily_refused: "Proposal temporarily <span class='assertive'>refused</span>",
          proposal_validated: "Proposal <span class='balanced'>validated</span>",
          proposal_refused: "Proposal <span class='assertive'>refused</span>",
          bethefirsttovote: 'Be the first to vote',
          youvoted: 'You voted',
          infavour: 'in favour',
          against: 'against',
          blank: 'blank',
          incomplete: 'incomplete',
          youhavenotvotedyet: 'you have not voted yet',
          votecanchange: 'You can change your choice at any time',
          changeofvote: 'Change of vote',
          anonymousvote: 'Anonymous vote',
          majorityrule: 'Majority rule',
          voter: 'voter',
          voter_plural: 'voters',
        },
        buttons: {
          disable: 'Disable',
          close: 'Close',
          backtoamendments: 'Back to amendments',
          openthevotes: 'Open the votes',
        },
        status: {
          amendable: 'Amendable',
          tovote: 'To vote',
          closed: 'Closed',
          resolved: 'Resolved',
          disabled: 'Disabled',
        },
      },
      debate: 'Debate',
    },
    schemas: {
      tibilletrest: {
        credits: {
          label: 'Credits',
        },
        numberCarte: {
          label: 'Number cart',
        },
      },
      loguseractionsrest: {
        credits: {
          label: 'Credits',
        },
        commentaire: {
          label: 'Comment',
        },
      },
      invitations: {
        childName: {
          label: 'name',
          placeholder: 'his name',
        },
        childEmail: {
          label: 'email',
          placeholder: 'his email',
        },
        childType: {
          label: 'Type',
          placeholder: 'Select one...',
          options: {
            citoyens: 'Citizens',
            organizations: 'Organizations',
          },
        },
        organizationType: {
          label: 'Organizations Type',
          placeholder: 'Select one...',
          options: {},
        },
        connectType: {
          label: 'Add as admin',
          placeholder: 'Add as admin',
        },
      },
      messages: {
        subject: {
          label: 'subject',
          placeholder: 'subject',
        },
        text: {
          label: 'message',
          placeholder: 'message',
        },
      },
      followrest: {
        invitedUserName: {
          label: 'name',
        },
        invitedUserEmail: {
          label: 'email',
        },
      },
      blockproposalsrest: {
        txtAmdt: {
          label: 'Your amendment',
        },
      },
      roomsrest: {
        name: {
          label: 'name',
        },
        description: {
          label: 'description',
        },
        roles: {
          label: 'Limit access by role (default: open to all)',
          placeholder: 'Select ...',
          options: {
            Organisateur: 'Organizer',
            Partenaire: 'Partner',
            Financeur: 'Financier',
            Président: 'President',
            Sponsor: 'Sponsor',
            Directeur: 'Director',
            Conférencier: 'Lecturer',
            Intervenant: 'Speaker',
          },
        },
      },
      actionsrest: {
        name: {
          label: 'name',
          placeholder: 'name',
        },
        description: {
          label: 'description',
          placeholder: 'description',
        },
        startDate: {
          label: 'start date',
        },
        endDate: {
          label: 'end date',
        },
        tags: {
          label: 'tags',
        },
        tagsText: {
          label: 'tags',
          placeholder: 'Tags use #',
        },
        assignText: {
          label: 'users',
          placeholder: 'Users use @',
        },
        urls: {
          label: 'Free information / urls',
        },
        max: {
          label: 'maximum',
          placeholder: 'maximum',
        },
        min: {
          label: 'minimum',
          placeholder: 'minimum',
        },
        credits: {
          label: 'credits',
        },
        options: {
          creditSharePorteur: {
            label: 'share credits among participants',
          },
          possibleStartActionBeforeStartDate: {
            label: 'Possibility to start the action before the start date',
          },
          creditAddPorteur: {
            label: 'Credits can be added by the participant (if only one participant)',
          },
        },
        milestoneId: {
          label: 'milestone',
          placeholder: 'Choose One...',
          options: {
            select: 'Choose One...',
          },
        },
      },
      proposalsrest: {
        title: {
          label: 'Name of your proposal',
        },
        description: {
          label: 'Your proposition',
          information: 'The final vote will be on the content of your proposal. For clarity, please provide additional information on your proposal in the next section.',
        },
        arguments: {
          label: 'Arguments',
          placeholder: 'Supplemental information, arguments, examples, demonstrations, etc',
        },
        amendementActivated: {
          label: 'Activate the amendments',
          information: 'Votes are disabled during the activated amendment period',
        },
        amendementDateEnd: {
          label: 'End of the amendment period (opening of votes)',
        },
        voteDateEnd: {
          label: 'End of voting period',
        },
        majority: {
          label: 'Majority rule (%)',
          information: 'Enter a value between 50% and 100%. Your proposal will need to collect more than <strong>__majority__%</strong> of votes to be validated',
        },
        voteAnonymous: {
          label: 'Votes anonymous',
          information: 'Would you like to keep the identity of the voters secret?',
        },
        voteCanChange: {
          label: 'Allow the change of vote',
          information: 'Would you like to allow voters to change their choice at will?',
        },
        tags: {
          label: 'Keywords',
        },
        urls: {
          label: 'Free information / urls',
        },
      },
      eventsrest: {
        name: {
          label: 'name',
        },
        shortDescription: {
          label: 'short description',
        },
        description: {
          label: 'description',
        },
        url: {
          label: 'url',
        },
        tags: {
          label: 'tags',
        },
        type: {
          label: 'type',
          placeholder: 'Select one...',
          options: {
            select: 'Choose One...',
          },
        },
        organizerType: {
          label: 'Organizer of the event',
          placeholder: 'Select one...',
          options: {
            citoyens: 'Me',
            organizations: 'Organizations',
            projects: 'Projects',
          },
        },
        organizerId: {
          label: 'Organizer of the event',
          placeholder: 'Select one...',
          options: {
            select: 'Choose One...',
          },
        },
        parentId: {
          label: 'parent of the event',
          placeholder: 'Not parent',
          options: {
            select: 'Choose One...',
          },
        },
        allDay: {
          label: 'all days',
        },
        startDate: {
          label: 'start date',
        },
        endDate: {
          label: 'end date',
        },
        country: {
          label: 'address country',
          placeholder: 'Select one...',
          options: {
            FR: 'France',
            GP: 'Guadeloupe',
            GF: 'French Guyana',
            MQ: 'Martinique',
            YT: 'Mayotte',
            NC: 'New Caledonia',
            RE: 'Reunion',
            PM: 'St Pierre and Miquelon',
            BE: 'Belgium',
          },
        },
        postalCode: {
          label: 'postal code',
        },
        city: {
          label: 'code insee',
          placeholder: 'Select one...',
          options: {
            select: 'Choose One...',
          },
        },
        streetAddress: {
          label: 'street address',
        },
        preferences: {
          label: 'preferences',
          isOpenData: {
            label: 'is Open Data',
          },
          isOpenEdition: {
            label: 'is Open Edition',
          },
        },
      },
      organizationsrest: {
        name: {
          label: 'name',
        },
        shortDescription: {
          label: 'short description',
        },
        description: {
          label: 'description',
        },
        type: {
          label: 'type',
          placeholder: 'Select one...',
          options: {
            select: 'Choose One...',
          },
        },
        role: {
          label: 'role',
          placeholder: 'Select one...',
          options: {
            admin: 'admin',
            member: 'member',
            creator: 'creator',
          },
        },
        email: {
          label: 'email',
        },
        url: {
          label: 'url',
        },
        fixe: {
          label: 'phone',
          placeholder: 'phone',
        },
        mobile: {
          label: 'mobile phone',
          placeholder: 'mobile phone',
        },
        fax: {
          label: 'fax',
          placeholder: 'fax',
        },
        tags: {
          label: 'tags',
        },
        country: {
          label: 'address country',
          placeholder: 'Select one...',
          options: {
            FR: 'France',
            GP: 'Guadeloupe',
            GF: 'French Guyana',
            MQ: 'Martinique',
            YT: 'Mayotte',
            NC: 'New Caledonia',
            RE: 'Reunion',
            PM: 'St Pierre and Miquelon',
            BE: 'Belgium',
          },
        },
        postalCode: {
          label: 'postal code',
        },
        city: {
          label: 'code insee',
          placeholder: 'Select one...',
          options: {
            select: 'Choose One...',
          },
        },
        streetAddress: {
          label: 'street address',
        },
        preferences: {
          label: 'preferences',
          isOpenData: {
            label: 'is Open Data',
          },
          isOpenEdition: {
            label: 'is Open Edition',
          },
        },
      },
      citoyensocecorest: {
        oceco: {
          notificationPush: {
            label: 'Notification push',
          },
          notificationEmail: {
            label: 'Notification Email',
          },
          notificationAllOrga: {
            label: 'Notification All',
          },
          pomodoroTimeWork: {
            label: 'Pomodoro time work',
          },
          pomodoroTimeShortRest: {
            label: 'Pomodoro time short rest',
          },
          pomodoroTimeLongRest: {
            label: 'Pomodoro time long rest',
          },
        },
      },
      projectocecorest: {
        oceco: {
          milestones: {
            label: 'milestones',
          },
        },
      },
      formsrest: {
        group: {
          label: 'group',
        },
        nature: {
          label: 'nature of the action',
        },
      },
      answersdepenserest: {
        group: {
          label: 'group',
          placeholder: 'group',
        },
        nature: {
          label: 'nature of the action',
          placeholder: 'nature of the action',
        },
        poste: {
          label: 'Expense item',
          placeholder: 'Expense item',
        },
        price: {
          label: 'Amount',
          placeholder: 'Amount',
        },
        days: {
          label: 'Days',
          placeholder: 'Days',
        },
        amount: {
          label: 'Amount Financed',
          placeholder: 'Amount Financed',
        },
        line: {
          label: 'Fund, envelope or budget mobilized',
          placeholder: 'Fund, envelope or budget mobilized',
        },
        name: {
          label: 'Name',
          placeholder: 'Name',
        },
        task: {
          label: 'Task',
          placeholder: 'Task',
        },
        email: {
          label: 'Email',
          placeholder: 'EMail',
        },
        communaute: {
          label: 'From the community ?',
          placeholder: 'From the community ?',
        },
        financeurId: {
          label: 'Financing',
          placeholder: 'Choose One...',
          options: {
            select: 'Choose One...',
          },
        },
        workType: {
          label: 'Type of work performed',
          placeholder: 'Type of work performed',
        },
        workerId: {
          label: 'Project manager',
          placeholder: 'Choose One...',
          options: {
            select: 'Choose One...',
          },
        },
        beneficiaryId: {
          label: 'Beneficiary',
          placeholder: 'Choose One...',
          options: {
            select: 'Choose One...',
          },
        },
        endDate: {
          label: 'End date',
          placeholder: 'End date',
        },
        assignText: {
          label: 'Assign to',
          placeholder: 'Assign to',
        },
      },
      organizationsocecorest: {
        oceco: {
          pole: {
            label: 'pole display',
          },
          organizationAction: {
            label: 'organization Action',
          },
          projectAction: {
            label: 'project Action',
          },
          eventAction: {
            label: 'event Action',
          },
          commentsAction: {
            label: 'comments Action',
          },
          spendView: {
            label: 'spend view',
          },
          spendNegative: {
            label: 'spend negative',
          },
          spendNegativeMax: {
            label: 'spend negative max',
          },
          memberAddAction: {
            label: 'member add action',
          },
          memberAuto: {
            label: 'member Auto',
          },
          memberValidationInviteAuto: {
            label: 'member validation invite auto',
          },
          contributorValidationInviteAuto: {
            label: 'contributor validation invite auto',
          },
          contributorAuto: {
            label: 'contributor Auto',
          },
          attendeAuto: {
            label: 'attende Auto',
          },
          agenda: {
            label: 'agenda display',
          },
          subOrganization: {
            label: 'sub organization display',
          },
          milestonesProject: {
            label: 'milestones project display',
          },
          organizationForms: {
            label: 'Activate organization calls for proposals',
          },
          projectForms: {
            label: 'Activate project calls for proposals',
          },
          notificationChat: {
            label: 'notification chat',
          },
          membersAdminProjectAdmin: {
            label: 'Access to member credit management in the project admin',
          },
          tibilletActive: {
            label: 'actived transfert tibillet',
          },
          tibillet: {
            apiKey: {
              label: 'apiKey',
            },
            server: {
              label: 'url server',
            },
            msgTransfert: {
              label: 'message',
            },
          },
          dashboardUser: {
            label: 'users view dashboard',
          },
          costum: {
            projects: {
              form: {
                geo: {
                  label: 'Project : Activate geo in the form',
                },
              },
            },
            events: {
              form: {
                geo: {
                  label: 'Event : Activate geo in the form',
                },
              },
            },
            actions: {
              form: {
                min: {
                  label: 'Action : Activate minimum number of participants',
                },
                max: {
                  label: 'Action : Activate maximum number of participants',
                },
                optionPodomoro: {
                  label: 'Action : Activate pomodoro',
                },
                optionTask: {
                  label: 'Action : Activate task',
                },
              },
            },
          },
          account: {
            label: 'Compte',
            textVotreCreditTemps: {
              label: 'Texte : Votre crédit temps',
            },
            textUnite: {
              label: 'Texte : unité',
            },
          },
          home: {
            label: 'Accueil',
            textTitre: {
              label: 'Texte : Titre',
            },
            textInfo: {
              label: 'Texte : Info',
            },
          },
          listingElementActions: {
            label: 'Element > Listing actions',
            tabProgrammes: {
              label: 'Texte tab programmés',
            },
            tabDepenses: {
              label: 'Texte tab dépenses',
            },
          },
          wallet: {
            label: 'Espace temps',
            textTitre: {
              label: 'Texte : Titre',
            },
            textInfo: {
              label: 'Texte : Info',
            },
            textBouton: {
              label: 'Texte : Bouton',
            },
            coupDeMain: {
              label: 'Coup de main',
              textTitre: {
                label: 'Texte : Titre',
              },
              textInfo: {
                label: 'Texte : Info',
              },
              textBouton: {
                label: 'Texte : Bouton',
              },
            },
            enAttente: {
              label: 'En attente',
              textTitre: {
                label: 'Texte : Titre',
              },
              textInfo: {
                label: 'Texte : Info',
              },
              textBouton: {
                label: 'Texte : Bouton',
              },
            },
            valides: {
              label: 'Validés',
              textTitre: {
                label: 'Texte : Titre',
              },
              textInfo: {
                label: 'Texte : Info',
              },
              textBouton: {
                label: 'Texte : Bouton',
              },
            },
          },
          subOrganizationsTxt: {
            label: 'Sub-organizations',
            textTitre: {
              label: 'Texte : Titre',
            },
          },
        },
      },
      projectsrest: {
        name: {
          label: 'name',
        },
        parentType: {
          label: 'creator of the project',
          placeholder: 'Select one...',
          options: {
            citoyens: 'Me',
            organizations: 'Organizations',
          },
        },
        parentId: {
          label: 'creator of the project',
          placeholder: 'Select one...',
          options: {
            select: 'Choose One...',
          },
        },
        shortDescription: {
          label: 'short description',
        },
        description: {
          label: 'description',
        },
        url: {
          label: 'url',
        },
        email: {
          label: 'email',
        },
        tags: {
          label: 'tags',
        },
        startDate: {
          label: 'start date',
        },
        endDate: {
          label: 'end date',
        },
        public: {
          label: 'Make the project publicly visible',
        },
        country: {
          label: 'address country',
          placeholder: 'Select one...',
          options: {
            FR: 'France',
            GP: 'Guadeloupe',
            GF: 'French Guyana',
            MQ: 'Martinique',
            YT: 'Mayotte',
            NC: 'New Caledonia',
            RE: 'Reunion',
            PM: 'St Pierre and Miquelon',
            BE: 'Belgium',
          },
        },
        postalCode: {
          label: 'postal code',
        },
        city: {
          label: 'code insee',
          placeholder: 'Select one...',
          options: {
            select: 'Choose One...',
          },
        },
        streetAddress: {
          label: 'street address',
        },
        preferences: {
          label: 'preferences',
          isOpenData: {
            label: 'is Open Data',
          },
          isOpenEdition: {
            label: 'is Open Edition',
          },
        },
      },
      news: {
        global: {
          name: {
            label: 'title',
          },
          text: {
            label: 'message',
            placeholder: 'message...',
          },
          tags: {
            label: 'tags',
          },
        },
        citoyens: {
          name: {
            label: 'title',
          },
          text: {
            label: 'message',
            placeholder: 'message...',
          },
          tags: {
            label: 'tags',
          },
          scope: {
            label: 'scope',
            placeholder: 'Select one...',
            options: {
              restricted: 'restricted',
              private: 'private',
              public: 'public',
            },
          },
        },
        projects: {
          name: {
            label: 'title',
          },
          text: {
            label: 'message',
            placeholder: 'message...',
          },
          tags: {
            label: 'tags',
          },
          scope: {
            label: 'scope',
            placeholder: 'Select one...',
            options: {
              private: 'private',
              public: 'public',
              restricted: 'restricted',
            },
          },
          targetIsAuthor: {
            label: 'Author',
            placeholder: 'Select one...',
            options: {
              false: 'Me',
              true: 'Project',
            },
          },
        },
        organizations: {
          name: {
            label: 'title',
          },
          text: {
            label: 'message',
            placeholder: 'message...',
          },
          tags: {
            label: 'tags',
          },
          scope: {
            label: 'scope',
            placeholder: 'Select one...',
            options: {
              private: 'private',
              public: 'public',
              restricted: 'restricted',
            },
          },
          targetIsAuthor: {
            label: 'Author',
            placeholder: 'Select one...',
            options: {
              false: 'Me',
              true: 'Organization',
            },
          },
        },
        events: {
          name: {
            label: 'title',
          },
          text: {
            label: 'message',
            placeholder: 'message...',
          },
          tags: {
            label: 'tags',
          },
          scope: {
            label: 'scope',
            placeholder: 'Select one...',
            options: {
              restricted: 'restricted',
            },
          },
        },
      },
      comments: {
        content: {
          label: 'message',
          placeholder: 'message',
        },
        text: {
          label: 'message',
          placeholder: 'message',
        },
        argval: {
          label: 'Type of review',
          placeholder: 'Select ...',
          options: {
            up: 'In favour',
            white: 'Neutral',
            down: 'Against',
          },
        },
      },
      sharerest: {
        comment: {
          label: 'message',
          placeholder: 'message',
        },
      },
      rolesrest: {
        roles: {
          label: 'Roles',
          placeholder: 'Select ...',
          options: {
            Organisateur: 'Organizer',
            Partenaire: 'Partner',
            Financeur: 'Financier',
            Président: 'President',
            Sponsor: 'Sponsor',
            Directeur: 'Director',
            Conférencier: 'Lecturer',
            Intervenant: 'Speaker',
          },
        },
      },
      global: {
        urgent: {
          label: 'urgent',
          placeholder: 'urgent',
        },
        name: {
          label: 'name',
          placeholder: 'name',
        },
        username: {
          label: 'user name',
          placeholder: 'user name',
        },
        shortDescription: {
          label: 'short description',
          placeholder: 'short description',
        },
        description: {
          label: 'description',
          placeholder: 'description',
        },
        url: {
          label: 'url',
          placeholder: 'url',
        },
        email: {
          label: 'email',
          placeholder: 'email',
        },
        fixe: {
          label: 'phone',
          placeholder: 'phone',
        },
        mobile: {
          label: 'mobile phone',
          placeholder: 'mobile phone',
        },
        fax: {
          label: 'fax',
          placeholder: 'fax',
        },
        tags: {
          label: 'tags',
        },
        birthDate: {
          label: 'birth date',
        },
        github: {
          label: 'github',
          placeholder: 'Link github',
        },
        telegram: {
          label: 'telegram',
          placeholder: 'Link telegram',
        },
        skype: {
          label: 'skype',
          placeholder: 'Link skype',
        },
        gpplus: {
          label: 'google plus',
          placeholder: 'Link google plus',
        },
        twitter: {
          label: 'twitter',
          placeholder: 'Link twitter',
        },
        facebook: {
          label: 'facebook',
          placeholder: 'Link facebook',
        },
        instagram: {
          label: 'instagram',
          placeholder: 'Link instagram',
        },
        type: {
          label: 'type',
          placeholder: 'Select one...',
          options: {
            select: 'Choose One...',
          },
        },
        role: {
          label: 'role',
          placeholder: 'Select one...',
          options: {
            admin: 'admin',
            member: 'member',
            creator: 'creator',
          },
        },
        avancement: {
          label: 'project progress',
          placeholder: 'Select one...',
          options: {
            idea: 'idea',
            concept: 'concept',
            started: 'started',
            development: 'development',
            testing: 'testing',
            mature: 'mature',
          },
        },
        allDay: {
          label: 'all days',
        },
        startDate: {
          label: 'start date',
        },
        endDate: {
          label: 'end date',
        },
        country: {
          label: 'address country',
          placeholder: 'Select one...',
          options: {
            FR: 'France',
            GP: 'Guadeloupe',
            GF: 'French Guyana',
            MQ: 'Martinique',
            YT: 'Mayotte',
            NC: 'New Caledonia',
            RE: 'Reunion',
            PM: 'St Pierre and Miquelon',
            BE: 'Belgium',
          },
        },
        postalCode: {
          label: 'postal code',
        },
        city: {
          label: 'code insee',
          placeholder: 'Select one...',
          options: {
            select: 'Choose One...',
          },
        },
        streetAddress: {
          label: 'street address',
        },
        preferences: {
          label: 'preferences',
          isOpenData: {
            label: 'is Open Data',
          },
          isOpenEdition: {
            label: 'is Open Edition',
          },
          email: {
            label: 'email',
            options: {
              public: 'public',
              private: 'private',
              hide: 'hide',
            },
          },
          locality: {
            label: 'locality',
            options: {
              public: 'public',
              private: 'private',
              hide: 'hide',
            },
          },
          phone: {
            label: 'phone',
            options: {
              public: 'public',
              private: 'private',
              hide: 'hide',
            },
          },
          directory: {
            label: 'directory',
            options: {
              public: 'public',
              private: 'private',
              hide: 'hide',
            },
          },
          birthDate: {
            label: 'birthDate',
            options: {
              public: 'public',
              private: 'private',
              hide: 'hide',
            },
          },
        },
      },
    },
  },
  // FRANCAIS
  fr: {
    message: {
      hello: 'bonjour monde',
      error: 'Erreur',
      email: 'Email',
      password: 'Mot de passe',
      login: "S'identifier",
      logout: 'Se déconnecter',
      createaccount: 'Créer un compte',
      forgotPassword: 'Mot de passe oublié',
      name: 'Nom',
      username: "Nom d'utilisateur",
      repassword: 'Vérification mot de passe',
      postcode: 'Code postal',
      Send: 'Envoyer',
      signin: "S'inscrire",
      alreadyCreatedAccount: 'Vous avez déja un compte',
      home: 'Acceuil',
      ChoiceofPole: 'Choix du pôle',
      ChoiceofProject: "Actions de l'organisation",
      Pending: 'En attente',
      invitedtoBeAdmin: 'vous a invité à devenir administrateur',
      invitedYou: 'vous a invité',
      actionsToBeDoneMoreThan: 'Actions <strong>toujours à faire</strong> qui ont plus de:',
      actionsToBeDoneNoContributorMoreThan: 'Actions <strong>toujours à faire</strong> et <strong>sans contributeur</strong> qui ont plus :',
      Actions: 'Actions',
      Member: 'Membre',
      MemberValidDescription: "Vous devez être un membre validé par un administrateur de l'organisation",
      ChoiceOfPole: 'Choix du pôle',
      ChoiceofPoleDescription: ' Sur cette page vous devrez choisir parmis les differents poles de votre organisation afin de voir les évenements et les actions qui en font partie. Si vous voulez voir les évenement par date merci de vous rendre dans "agenda" ',
      ChoiceOfProjects: "Les actions de l'organisation",
      ChoiceOfProjectsDescription: 'Toutes les actions de votre organisation, des projets ou des évènements',
      Dashboard: 'Dashboard',
      DashboardUserConfig: 'Configuration dashboard utilisateur',
      allfields: 'Vous devez renseigner tous les champs',
      nameTooShort: 'Votre nom est trop court',
      userNameTooShort: "Votre nom d'utilisateur est trop court",
      userNameIncorrect: "Le nom d'utilisateur est incorrect : Seul les caractères A-Z, a-z, 0-9 et '-' sont acceptés.",
      passwordTooShort: 'Votre mot de passe est trop court',
      emailNotValid: 'Email non valide"',
      notSamePassword: "Vous n'avez pas saisis le même mot de passe",
      memberActions: 'Actions des Membres',
      withContributors: 'avec contributeurs :',
      withoutContributors: 'sans contributeurs :',
      whereIamContributors: 'ou je suis contributeurs :',
      byTags: 'par tags :',
      byMilestone: 'par jalon :',
      byProjects: 'par projets :',
      veryOld: 'trés ancien :',
      descendingSort: 'trie descendant',
      Sort: 'Trier',
      sortProjectsBy: 'Trier les projets par :',
      sortEventsBy: 'Trier les évènement par :',
      sortActionsBy: 'Trier les actions par :',
      numberMonth: '__count__ mois',
      filterByTags: 'Filtrer par tags',
      filterByMilestone: 'Filtrer par jalon',
      filterByUser: 'Filtrer par utilisateur',
      filterActions: 'Filtrer les actions',
      searchActionPlaceholder: 'Chercher une action par nom, #tag, :projet...',
      searchAnswerPlaceholder: 'Chercher une proposition par nom, #tag, !statut...',
      OrganizationOCECO: 'Organisation OCECO',
      ActivateOrganizationOceco: 'Activer une organisation dans oceco',
      Notifications: 'Notifications',
      ChangeOrganization: "Changer d'organisation",
      TimeSpace: 'Espace Temps',
      BecomeMemberOrga: "Devenir membre de l'organisation",
      NoLongerMemberOrga: "Ne plus être membre de l'organisation",
      OrganizationActions: "Actions de l'organisation",
      Canceled: 'Annulée',
      Finished: 'Terminée',
      You_will_earn_count_credit: 'Vous gagnerez <strong>__count__</strong> crédit',
      You_will_earn_count_credit_plural: 'Vous gagnerez <strong>__count__</strong> crédits',
      You_will_be_deducted_from_count_credit: 'Vous serez prélevé de __count__ crédit',
      You_will_be_deducted_from_count_credit_plural: 'Vous serez prélevé de __count__ crédits',
      Start_on_day: 'Début le __day__',
      Duration_in_hours: 'Durée __hours__ H',
      number_of_participant: 'Il y a __count__ participant',
      number_of_participant_plural: 'Il y a __count__ participants',
      minimum_participant: 'Pour un besoin minimum de __count__ participant',
      minimum_participant_plural: 'Pour un besoin minimum de __count__ participants',
      maximum_participant: 'Et un maximum de __count__ participant',
      maximum_participant_plural: 'Et un maximum de __count__ participants',
      Project: 'Projet',
      start: 'début',
      end: 'fin',
      duration: 'Durée',
      distance: 'Distance',
      Hide_action_count: 'Cacher __count__ action',
      Hide_action_count_plural: 'Cacher __count__ actions',
      See_action_count: 'Voir __count__ action',
      See_action_count_plural: 'Voir __count__ actions',
      EventActions: "Actions de l'événement",
      begin: 'commence',
      Credits: 'Crédits',
      StartActionNow: "Démarrer l'action maintenant",
      AlreadyComplete: 'Déja complet',
      Spent_count: 'Dépenser __count__',
      InProgress: 'En cours',
      ProjectActions: 'Actions du projet',
      ProjectEvents: 'Evenements du projet',
      NoUpcomingEvent: "Pas d'évènement à venir",
      YouTimeCredit: 'Votre crédit temps',
      ShareUrl: 'Partager url',
      Use_the_copy_function_when_you_click_on_the_field_where_there_is_the_link: 'Utiliser la fonction de copie quand vous cliquer sur le champ ou il y a le lien',
      Copy: 'Copier',
      url: 'url',
      share: 'Partager',
      textTitre: 'Texte : Titre',
      textInfo: 'Texte : Info',
      textBouton: 'Texte : Bouton',
      goBack: 'Retour en arrière',
      citoyens: 'citoyens',
      EditCitoyen: 'Modifier votre profil',
      edit: 'Modifier',

    },
    sortModal: {
      name: 'nom',
      activity_date: "date d'activités",
      start_date: 'date de début',
      end_date: 'date de fin',
      creation_date: 'date de création',
      contributor: 'contributeur',
      comment: 'commentaire',
      credits: 'crédits',
    },
    dda: {
      resolution: {
        resolution: 'Résolution',
        adopted: "La <strong>résolution</strong> a été prise : la proposition est <span class='balanced'>validée</span>",
        refused: "La <strong>résolution</strong> a été refusée : la proposition est <span class='assertive'>refusée</span>",
        endofvote: 'Fin du vote',
        showproposaldetail: 'Afficher le detail de la proposition',
        showresolution: 'Afficher la résolution',
        resultstitle: 'Résultats',
      },
      actions: {
        action: 'Action',
        actionstartenddate: 'Action à réaliser du __start__ au __end__',
        spentstartenddate: 'Dépense à réaliser du __start__ au __end__',
        endofaction: "Fin de l'action",
        theytakeaction: 'Ils participent à cette action',
        youparticipatingaction: 'Vous participez à cette action',
        wantparticipateaction: 'Je veux participer à cette action',
        noparticipants: 'Pas de participants',
        discussion: 'Discussion',
        buttons: {
          disable: 'Désactivée',
          close: 'Terminée',
        },
        status: {
          inprogress: 'En cours',
          completed: 'Terminée',
          todo: 'À faire',
          disabled: 'Désactivée',
        },
      },
      proposal: {
        proposal: 'Proposition',
        argumentstitle: "Compléments d'informations, argumentations, exemples, démonstrations, etc.",
        amendments: {
          title: 'Amendements',
          text: "Proposition soumise aux amendements jusqu'au",
          endofamendments: 'Fin des amendements',
          proposetext: 'Vous pouvez proposer des amendements et voter les amendements proposés par les autres utilisateurs',
          listofamendmentstemporarily: 'Liste des amendements temporairement validés ·  Majorité : 50%',
          amendmentsdeactivated: 'Amendements désactivés',
          noamendments: "Pas d'amendements",
          validated: 'validé',
          validated_temporarily: 'temporairement validé',
          refused: 'refusé',
          refused_temporarily: 'temporairement refusé',
        },
        tovote: {
          title: 'Voter',
          text: "Proposition soumise aux votes jusqu'au",
          endofvotes: 'Fin des votes',
          proposetext: 'You can propose amendments and vote on amendments proposed by other users',
          listofamendmentsvalidated: 'Liste des amendements validés · Majorité',
          votesdisabled: 'votes désactivés',
          proposal_temporarily_validated: "Proposition temporairement <span class='balanced'>validée</span>",
          proposal_temporarily_refused: "Proposition temporairement <span class='assertive'>refusée</span>",
          proposal_validated: "Proposition <span class='balanced'>validée</span>",
          proposal_refused: "Proposition <span class='assertive'>refusée</span>",
          bethefirsttovote: 'Soyez le premier à voter',
          youvoted: 'Vous avez voté',
          infavour: 'pour',
          against: 'contre',
          blank: 'blanc',
          incomplete: 'incomplet',
          youhavenotvotedyet: "vous n'avez pas encore voté",
          votecanchange: 'Vous pouvez changer votre choix à tout moment',
          changeofvote: 'Changement de vote',
          anonymousvote: 'Vote anonyme',
          majorityrule: 'Règle de majorité',
          voter: 'votant',
          voter_plural: 'votants',
        },
        buttons: {
          disable: 'Désactiver',
          close: 'Fermer',
          backtoamendments: 'Retour aux amendements',
          openthevotes: 'Ouvrir les votes',
        },
        status: {
          amendable: 'Amendable',
          tovote: 'À voter',
          closed: 'Fermée',
          resolved: 'Résolue',
          disabled: 'Désactivée',
        },
      },
      debate: 'Débat',
    },
    schemas: {
      loguseractionsrest: {
        credits: {
          label: 'Credits',
        },
        commentaire: {
          label: 'Commentaires',
        },
      },
      tibilletrest: {
        credits: {
          label: 'Nombres de Crédits',
        },
        numberCarte: {
          label: 'Numéro de carte',
        },
      },
      invitations: {
        childName: {
          label: 'Nom',
          placeholder: 'son nom',
        },
        childEmail: {
          label: 'Email',
          placeholder: 'son email',
        },
        childType: {
          label: 'Type',
          placeholder: 'Sélectionnez un...',
          options: {
            citoyens: 'un citoyen',
            organizations: 'une organisation',
          },
        },
        organizationType: {
          label: 'Organizations Type',
          placeholder: 'Sélectionnez un...',
          options: {},
        },
        connectType: {
          label: 'Ajouter en tant qu’admin',
          placeholder: 'Ajouter en tant qu’admin',
        },
      },
      messages: {
        subject: {
          label: 'sujet',
          placeholder: 'sujet',
        },
        text: {
          label: 'message',
          placeholder: 'message',
        },
      },
      followrest: {
        invitedUserName: {
          label: 'Nom',
        },
        invitedUserEmail: {
          label: 'Email',
        },
      },
      blockproposalsrest: {
        txtAmdt: {
          label: 'Votre amendement',
        },
      },
      roomsrest: {
        name: {
          label: 'Nom',
        },
        description: {
          label: 'Description',
        },
        roles: {
          label: "Limiter l'accès par rôle (par défaut : ouvert à tous)",
          placeholder: 'Sélectionnez ...',
          options: {
            Organisateur: 'Organisateur',
            Partenaire: 'Partenaire',
            Financeur: 'Financeur',
            Président: 'Président',
            Sponsor: 'Sponsor',
            Directeur: 'Directeur',
            Conférencier: 'Conférencier',
            Intervenant: 'Intervenant',
          },
        },
      },
      actionsrest: {
        name: {
          label: 'nom',
          placeholder: 'nom de votre action',
        },
        description: {
          label: 'description',
          placeholder: 'description de votre action (option)',
        },
        startDate: {
          label: 'Date de début',
        },
        endDate: {
          label: 'Date de fin',
        },
        tags: {
          label: 'Mots clés',
        },
        tagsText: {
          label: 'Tags',
          placeholder: 'Tags utiliser #',
        },
        assignText: {
          label: 'Assigner utilisateurs',
          placeholder: 'Pour assigner utiliser @',
        },
        urls: {
          label: 'Informations libres / urls',
        },
        max: {
          label: 'Participant maximum',
          placeholder: 'Participants maximum',
        },
        min: {
          label: 'Participant minimum',
          placeholder: 'Participants minimum',
        },
        credits: {
          label: 'Nombre de crédits',
        },
        options: {
          creditSharePorteur: {
            label: 'Partager les crédits entre les participants (minimum 1 crédits par participants)',
          },
          possibleStartActionBeforeStartDate: {
            label: "Possibilité de démarrer l'action avant la date de début",
          },
          creditAddPorteur: {
            label: 'Crédits ajoutés par le participant (si un seul participant)',
          },
        },
        milestoneId: {
          label: 'Jalon (milestone)',
          placeholder: 'Sélectionnez un...',
          options: {
            select: 'Sélectionnez un...',
          },
        },
      },
      proposalsrest: {
        title: {
          label: 'Nom de votre proposition',
        },
        description: {
          label: 'Votre proposition',
          information: 'Le vote final portera sur le contenu de votre proposition. Pour plus de clarté, veuillez fournir des informations supplémentaires sur votre proposition dans la section suivante.',
        },
        arguments: {
          label: "Compléments d'informations",
          placeholder: "Compléments d'informations, argumentations, exemples, démonstrations, etc",
        },
        amendementActivated: {
          label: 'Activer les amendements',
          information: "Les votes sont désactivés pendant la période d'amendement activée",
        },
        amendementDateEnd: {
          label: "Fin de la période d'amendement (ouverture des votes)",
        },
        voteDateEnd: {
          label: 'Fin de la période de vote',
        },
        majority: {
          label: 'Règle de majorité (%)',
          information: 'Entrez une valeur comprise entre 50% et 100%. Votre proposition devra collecter plus de <strong>__majority__%</strong> des votes favorables pour être validée',
        },
        voteAnonymous: {
          label: 'Votes anonyme',
          information: "Voulez-vous garder l'identité des électeurs secrète ?",
        },
        voteCanChange: {
          label: 'Autoriser le changement de vote',
          information: 'Voulez-vous permettre aux électeurs de changer leur choix à volonté ?',
        },
        tags: {
          label: 'Mots clés',
        },
        urls: {
          label: 'Informations libres / urls',
        },
      },
      eventsrest: {
        name: {
          label: 'Nom',
        },
        shortDescription: {
          label: 'Description courte',
        },
        description: {
          label: 'Description',
        },
        url: {
          label: 'Url',
        },
        tags: {
          label: 'Tags',
        },
        type: {
          label: 'Type',
          placeholder: 'Sélectionnez un...',
          options: {
            select: 'Sélectionnez un...',
          },
        },
        organizerType: {
          label: "type d'Organisateur",
          placeholder: 'Sélectionnez un...',
          options: {
            citoyens: 'Moi',
            organizations: 'une organisation',
            projects: 'un projet',
          },
        },
        organizerId: {
          label: 'Projet',
          placeholder: 'Sélectionnez un...',
          options: {
            select: 'Sélectionnez un...',
          },
        },
        parentId: {
          label: "parent de l'événement",
          placeholder: 'pas de parent',
          options: {
            select: 'Sélectionnez un...',
          },
        },
        allDay: {
          label: 'Toute la journée',
        },
        startDate: {
          label: 'Date de début',
        },
        endDate: {
          label: 'Date de fin',
        },
        country: {
          label: 'Pays',
          placeholder: 'Sélectionnez un...',
          options: {
            FR: 'France',
            GP: 'Guadeloupe',
            GF: 'Guyanne Française',
            MQ: 'Martinique',
            YT: 'Mayotte',
            NC: 'Nouvelle-Calédonie',
            RE: 'Réunion',
            PM: 'St Pierre et Miquelon',
            BE: 'Belgique',
          },
        },
        postalCode: {
          label: 'Code postal',
        },
        city: {
          label: 'Ville',
          placeholder: 'Sélectionnez un...',
          options: {
            select: 'Sélectionnez un...',
          },
        },
        streetAddress: {
          label: 'Adresse',
        },
        preferences: {
          label: 'Préférences',
          isOpenData: {
            label: 'Open Data',
          },
          isOpenEdition: {
            label: 'Open Edition',
          },
        },
      },
      organizationsrest: {
        name: {
          label: 'Nom',
        },
        shortDescription: {
          label: 'Description courte',
        },
        description: {
          label: 'Description',
        },
        type: {
          label: 'Type',
          placeholder: 'Sélectionnez un...',
          options: {
            select: 'Sélectionnez un...',
          },
        },
        role: {
          label: 'Role',
          placeholder: 'Sélectionnez un...',
          options: {
            admin: 'Administrateur',
            member: 'Membre',
            creator: 'Juste un citoyen',
          },
        },
        email: {
          label: 'Email',
        },
        url: {
          label: 'Url',
        },
        fixe: {
          label: 'Téléphone fixe',
          placeholder: 'téléphone fixe',
        },
        mobile: {
          label: 'Téléphone mobile',
          placeholder: 'téléphone mobile',
        },
        fax: {
          label: 'Fax',
          placeholder: 'fax',
        },
        tags: {
          label: 'Tags',
        },
        country: {
          label: 'Pays',
          placeholder: 'Sélectionnez un...',
          options: {
            FR: 'France',
            GP: 'Guadeloupe',
            GF: 'Guyanne Française',
            MQ: 'Martinique',
            YT: 'Mayotte',
            NC: 'Nouvelle-Calédonie',
            RE: 'Réunion',
            PM: 'St Pierre et Miquelon',
            BE: 'Belgique',
          },
        },
        postalCode: {
          label: 'Code postal',
        },
        city: {
          label: 'Ville',
          placeholder: 'Sélectionnez un...',
          options: {
            select: 'Sélectionnez un...',
          },
        },
        streetAddress: {
          label: 'Adresse',
        },
        preferences: {
          label: 'Préférences',
          isOpenData: {
            label: 'Open Data',
          },
          isOpenEdition: {
            label: 'Open Edition',
          },
        },
      },
      citoyensocecorest: {
        oceco: {
          notificationPush: {
            label: 'Notification Push',
          },
          notificationEmail: {
            label: 'Notification Email',
          },
          notificationAllOrga: {
            label: 'Recevoir toutes les notifications',
          },
          pomodoroTimeWork: {
            label: 'Pomodoro : cycle temps de travail en minutes',
          },
          pomodoroTimeShortRest: {
            label: 'Pomodoro : cycle temps de repo court en minutes',
          },
          pomodoroTimeLongRest: {
            label: 'Pomodoro : cycle temps de repo long en minutes',
          },
        },
      },
      projectocecorest: {
        oceco: {
          milestones: {
            label: 'Ajouter des jalons au projet (milestones)',
          },
        },
      },
      organizationsocecorest: {
        oceco: {
          pole: {
            label: 'Affichage par pôle',
          },
          organizationAction: {
            label: 'Activer action organisation',
          },
          projectAction: {
            label: 'Activer action projet',
          },
          eventAction: {
            label: 'Activer action évenement',
          },
          commentsAction: {
            label: 'Activer commentaires',
          },
          spendView: {
            label: 'Afficher le listing dépenses',
          },
          spendNegative: {
            label: 'Autoriser les dépense négatives',
          },
          spendNegativeMax: {
            label: 'Dépense négatives maximum',
          },
          memberAddAction: {
            label: 'Autoriser les membres à ajouter des actions',
          },
          memberAuto: {
            label: 'Ajouter des membres automatiquement',
          },
          memberValidationInviteAuto: {
            label: "Valider automatiquement les invitations venant d'un admin pour être membre",
          },
          contributorValidationInviteAuto: {
            label: "Valider automatiquement les invitations venant d'un admin pour être contributeur",
          },
          contributorAuto: {
            label: 'Ajouter des contributeurs au projet automatiquement (à la participation ou assignation à une action)',
          },
          attendeAuto: {
            label: "Ajouter des participants à l'événement automatiquement (à la participation ou assignation à une action)",
          },
          agenda: {
            label: 'Activer agenda évenement',
          },
          subOrganization: {
            label: 'Activer les sous-organisations',
          },
          milestonesProject: {
            label: 'Activer les jalons (milestones) sur les projets',
          },
          organizationForms: {
            label: "Activer les appels à propositions sur l'organisation",
          },
          projectForms: {
            label: 'Activer les appels à propositions sur les projets',
          },
          notificationChat: {
            label: 'Notification chat',
          },
          membersAdminProjectAdmin: {
            label: 'Accès à la gestion de crédits des membres au admin de projets',
          },
          tibilletActive: {
            label: 'activer le transfert tibillet',
          },
          tibillet: {
            apiKey: {
              label: 'apiKey',
            },
            server: {
              label: 'url server',
            },
            msgTransfert: {
              label: 'Message affiché au transfert',
            },
          },
          dashboardUser: {
            label: 'users view dashboard',
          },
          costum: {
            projects: {
              form: {
                geo: {
                  label: 'Projet : Activer geo dans le formulaire',
                },
              },
            },
            events: {
              form: {
                geo: {
                  label: 'Evenement : Activer geo dans le formulaire',
                },
              },
            },
            actions: {
              form: {
                min: {
                  label: 'Action : Activer nb participants mini dans le formulaire',
                },
                max: {
                  label: 'Action : Activer nb participants max dans le formulaire',
                },
                optionPodomoro: {
                  label: 'Action : Activer pomodoro',
                },
                optionTask: {
                  label: 'Action : Activer tâche',
                },
              },
            },
          },
          account: {
            label: 'Compte',
            textVotreCreditTemps: {
              label: 'Texte : Votre crédit temps',
            },
            textUnite: {
              label: 'Texte : unité',
            },
          },
          home: {
            label: 'Accueil',
            textTitre: {
              label: 'Texte : Titre',
            },
            textInfo: {
              label: 'Texte : Info',
            },
          },
          listingElementActions: {
            label: 'Element > Listing actions',
            tabProgrammes: {
              label: 'Texte tab programmés',
            },
            tabDepenses: {
              label: 'Texte tab dépenses',
            },
          },
          wallet: {
            label: 'Espace temps',
            textTitre: {
              label: 'Texte : Titre',
            },
            textInfo: {
              label: 'Texte : Info',
            },
            textBouton: {
              label: 'Texte : Bouton',
            },
            coupDeMain: {
              label: 'Coup de main',
              textTitre: {
                label: 'Texte : Titre',
              },
              textInfo: {
                label: 'Texte : Info',
              },
              textBouton: {
                label: 'Texte : Bouton',
              },
            },
            enAttente: {
              label: 'En attente',
              textTitre: {
                label: 'Texte : Titre',
              },
              textInfo: {
                label: 'Texte : Info',
              },
              textBouton: {
                label: 'Texte : Bouton',
              },
            },
            valides: {
              label: 'Validés',
              textTitre: {
                label: 'Texte : Titre',
              },
              textInfo: {
                label: 'Texte : Info',
              },
              textBouton: {
                label: 'Texte : Bouton',
              },
            },
          },
          subOrganizationsTxt: {
            label: 'Sub-organizations',
            textTitre: {
              label: 'Texte : Titre',
            },
          },
        },
      },
      formsrest: {
        group: {
          label: 'Groupe',
        },
        nature: {
          label: "Nature de l'action",
        },
      },
      answersdepenserest: {
        group: {
          label: 'Groupe',
          placeholder: 'Groupe',
        },
        nature: {
          label: "Nature de l'action",
          placeholder: "Nature de l'action",
        },
        poste: {
          label: 'Poste de dépense',
          placeholder: 'Poste de dépense',
        },
        price: {
          label: 'Montant',
          placeholder: 'Montant',
        },
        days: {
          label: 'Nombre de jours',
          placeholder: 'Nombre de jours',
        },
        amount: {
          label: 'Montant Financé',
          placeholder: 'Montant Financé',
        },
        line: {
          label: 'Fonds, enveloppe ou budget mobilisé',
          placeholder: 'Fonds, enveloppe ou budget mobilisé',
        },
        name: {
          label: 'Nom',
          placeholder: 'Nom',
        },
        task: {
          label: 'Tâche',
          placeholder: 'Tâche',
        },
        email: {
          label: 'Email',
          placeholder: 'EMail',
        },
        communaute: {
          label: 'De la communauté ?',
          placeholder: 'De la communauté ?',
        },
        financeurId: {
          label: 'Financeur',
          placeholder: 'Sélectionnez un...',
          options: {
            select: 'Sélectionnez un...',
          },
        },
        workType: {
          label: 'Type de travail effectué',
          placeholder: 'Type de travail effectué',
        },
        workerId: {
          label: "Maître d'oeuvre",
          placeholder: 'Sélectionnez un...',
          options: {
            select: 'Sélectionnez un...',
          },
        },
        beneficiaryId: {
          label: 'Bénéficiaire',
          placeholder: 'Sélectionnez un...',
          options: {
            select: 'Sélectionnez un...',
          },
        },
        endDate: {
          label: 'Date de fin',
          placeholder: 'Date de fin',
        },
        assignText: {
          label: 'Assigner à',
          placeholder: 'Assigner à',
        },
      },
      projectsrest: {
        name: {
          label: 'Nom',
        },
        parentType: {
          label: 'Createur type',
          placeholder: 'Sélectionnez un...',
          options: {
            citoyens: 'Moi',
            organizations: 'Organisations',
          },
        },
        parentId: {
          label: 'Createur',
          placeholder: 'Sélectionnez un...',
          options: {
            select: 'Sélectionnez un...',
          },
        },
        shortDescription: {
          label: 'Description courte',
        },
        description: {
          label: 'Description',
        },
        url: {
          label: 'Url',
        },
        email: {
          label: 'Email',
        },
        tags: {
          label: 'Tags',
        },
        startDate: {
          label: 'Date de début',
        },
        endDate: {
          label: 'Date de fin',
        },
        public: {
          label: 'Rendre le projet visible publiquement',
        },
        country: {
          label: 'Pays',
          placeholder: 'Sélectionnez un...',
          options: {
            FR: 'France',
            GP: 'Guadeloupe',
            GF: 'Guyanne Française',
            MQ: 'Martinique',
            YT: 'Mayotte',
            NC: 'Nouvelle-Calédonie',
            RE: 'Réunion',
            PM: 'St Pierre et Miquelon',
            BE: 'Belgique',
          },
        },
        postalCode: {
          label: 'Code postal',
        },
        city: {
          label: 'Ville',
          placeholder: 'Sélectionnez un...',
          options: {
            select: 'Sélectionnez un...',
          },
        },
        streetAddress: {
          label: 'Adresse',
        },
        preferences: {
          label: 'Préférences',
          isOpenData: {
            label: 'Open Data',
          },
          isOpenEdition: {
            label: 'Open Edition',
          },
        },
      },
      news: {
        global: {
          name: {
            label: 'title',
          },
          text: {
            label: 'Message',
            placeholder: 'message...',
          },
          tags: {
            label: 'Tags',
          },
        },
        citoyens: {
          name: {
            label: 'title',
          },
          text: {
            label: 'Message',
            placeholder: 'message...',
          },
          tags: {
            label: 'Tags',
          },
          scope: {
            label: 'Visibilité',
            placeholder: 'Sélectionnez un...',
            options: {
              restricted: 'Mon réseau',
              private: 'Privé',
              public: 'Public',
            },
          },
        },
        projects: {
          name: {
            label: 'title',
          },
          text: {
            label: 'Message',
            placeholder: 'message...',
          },
          tags: {
            label: 'Tags',
          },
          scope: {
            label: 'Visibilité',
            placeholder: 'Sélectionnez un...',
            options: {
              private: 'Privé',
              public: 'Public',
              restricted: ' Mon réseau',
            },
          },
          targetIsAuthor: {
            label: 'Auteur',
            placeholder: 'Sélectionnez un...',
            options: {
              false: 'Moi',
              true: 'Le projet',
            },
          },
        },
        organizations: {
          name: {
            label: 'title',
          },
          text: {
            label: 'Message',
            placeholder: 'message...',
          },
          tags: {
            label: 'Tags',
          },
          scope: {
            label: 'Visibilité',
            placeholder: 'Sélectionnez un...',
            options: {
              private: 'Privé',
              public: 'Public',
              restricted: ' Mon réseau',
            },
          },
          targetIsAuthor: {
            label: 'Auteur',
            placeholder: 'Sélectionnez un...',
            options: {
              false: 'Moi',
              true: "L'organisation",
            },
          },
        },
        events: {
          name: {
            label: 'title',
          },
          text: {
            label: 'Message',
            placeholder: 'message...',
          },
          tags: {
            label: 'Tags',
          },
          scope: {
            label: 'Visibilité',
            placeholder: 'Sélectionnez un...',
            options: {
              restricted: ' Mon réseau',
            },
          },
        },
      },
      comments: {
        content: {
          label: 'message',
          placeholder: 'commentaire...',
        },
        text: {
          label: 'message',
          placeholder: 'commentaire...',
        },
        argval: {
          label: "Type d'avis",
          placeholder: 'Sélectionnez un...',
          options: {
            up: 'Pour',
            white: 'Neutre',
            down: 'Contre',
          },
        },
      },
      sharerest: {
        comment: {
          label: 'message',
          placeholder: 'commentaire...',
        },
      },
      rolesrest: {
        roles: {
          label: 'Rôles',
          placeholder: 'Sélectionnez ...',
          options: {
            Organisateur: 'Organisateur',
            Partenaire: 'Partenaire',
            Financeur: 'Financeur',
            Président: 'Président',
            Sponsor: 'Sponsor',
            Directeur: 'Directeur',
            Conférencier: 'Conférencier',
            Intervenant: 'Intervenant',
          },
        },
      },
      global: {
        urgent: {
          label: 'Urgent',
          placeholder: 'Urgent',
        },
        name: {
          label: 'Nom',
          placeholder: 'nom',
        },
        username: {
          label: 'Pseudo',
          placeholder: 'Pseudo',
        },
        shortDescription: {
          label: 'Description courte',
          placeholder: 'Description courte',
        },
        description: {
          label: 'Description',
          placeholder: 'Description',
        },
        email: {
          label: 'Email',
          placeholder: 'email',
        },
        fixe: {
          label: 'Téléphone fixe',
          placeholder: 'téléphone fixe',
        },
        mobile: {
          label: 'Téléphone mobile',
          placeholder: 'téléphone mobile',
        },
        fax: {
          label: 'Fax',
          placeholder: 'fax',
        },
        url: {
          label: 'Url',
          placeholder: 'url',
        },
        tags: {
          label: 'Tags',
        },
        birthDate: {
          label: "Date d'anniversaire",
        },
        allDay: {
          label: 'Toute la journée',
        },
        startDate: {
          label: 'Date de début',
        },
        endDate: {
          label: 'Date de fin',
        },
        type: {
          label: 'Type',
          placeholder: 'Sélectionnez un...',
          options: {
            select: 'Sélectionnez un...',
          },
        },
        role: {
          label: 'Role',
          placeholder: 'Sélectionnez un...',
          options: {
            admin: 'Administrateur',
            member: 'Membre',
            creator: 'Juste un citoyen',
          },
        },
        avancement: {
          label: 'Avancement',
          placeholder: 'Sélectionnez un...',
          options: {
            idea: 'idée',
            concept: 'concept',
            started: 'démarrer',
            development: 'développement',
            testing: 'expérimentation',
            mature: 'mature',
          },
        },
        github: {
          label: 'github',
          placeholder: 'Lien github',
        },
        telegram: {
          label: 'telegram',
          placeholder: 'Speudo telegram',
        },
        skype: {
          label: 'skype',
          placeholder: 'Lien skype',
        },
        gpplus: {
          label: 'google plus',
          placeholder: 'Lien google plus',
        },
        twitter: {
          label: 'twitter',
          placeholder: 'Lien twitter',
        },
        facebook: {
          label: 'facebook',
          placeholder: 'Lien facebook',
        },
        instagram: {
          label: 'instagram',
          placeholder: 'Lien instagram',
        },
        country: {
          label: 'Pays',
          placeholder: 'Sélectionnez un...',
          options: {
            FR: 'France',
            GP: 'Guadeloupe',
            GF: 'Guyanne Française',
            MQ: 'Martinique',
            YT: 'Mayotte',
            NC: 'Nouvelle-Calédonie',
            RE: 'Réunion',
            PM: 'St Pierre et Miquelon',
            BE: 'Belgique',
          },
        },
        postalCode: {
          label: 'Code postal',
        },
        city: {
          label: 'Ville',
          placeholder: 'Sélectionnez un...',
          options: {
            select: 'Sélectionnez un...',
          },
        },
        streetAddress: {
          label: 'Adresse',
        },
        preferences: {
          label: 'Préférences',
          isOpenData: {
            label: 'Open Data',
          },
          isOpenEdition: {
            label: 'Open Edition',
          },
          email: {
            label: 'Mon e-mail',
            options: {
              public: 'public',
              private: 'privé',
              hide: 'masqué',
            },
          },
          locality: {
            label: 'Adresse',
            options: {
              public: 'public',
              private: 'privé',
              hide: 'masqué',
            },
          },
          phone: {
            label: 'Mon téléphone',
            options: {
              public: 'public',
              private: 'privé',
              hide: 'masqué',
            },
          },
          directory: {
            label: 'Mon répertoire',
            options: {
              public: 'public',
              private: 'privé',
              hide: 'masqué',
            },
          },
          birthDate: {
            label: 'Date de naissance',
            options: {
              public: 'public',
              private: 'privé',
              hide: 'masqué',
            },
          },
        },
      },
    },
  },
};

export default messages;
