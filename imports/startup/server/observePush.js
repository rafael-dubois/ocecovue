/* eslint-disable object-shorthand */
/* global Assets */
import { Meteor } from 'meteor/meteor';
import { _ } from 'meteor/underscore';
import { Push } from 'meteor/raix:push';
import MJML from 'meteor/djabatav:mjml';
import { Jobs } from 'meteor/wildhart:jobs';
import { Mongo } from 'meteor/mongo';
import i18n from 'meteor/universe:i18n';

import { ActivityStream } from '../../api/collection/activitystream.js';
import { notifyDisplay } from '../../api/helpers.js';
import { Citoyens } from '../../api/collection/citoyens.js';
import { Actions } from '../../api/collection/actions.js';

import log from './logger.js';

if (Meteor.isDevelopment) {
  Push.debug = true;
  Jobs.configure({
    log: false,
  });
} else {
  Jobs.configure({
    log: false,
  });
}

const userLangageObs = (userId) => {
  const user = Meteor.users.findOne({ _id: userId }, { fields: { 'profile.language': 1, 'profile.pixelhumain.language': 1 } });
  if (user && user.profile && user.profile.pixelhumain && user.profile.pixelhumain.language) {
    return user.profile.pixelhumain.language;
  }
  if (user && user.profile && user.profile.language) {
    return user.profile.language;
  }
};

const pushUser = (title, text, query, badge) => {
  const citoyensListEmail = Citoyens.find({ _id: new Mongo.ObjectID(query.userId) }, { fields: { oceco: 1 } });
  citoyensListEmail.forEach((citoyen) => {
    const notificationPush = (citoyen && !citoyen.oceco) ? true : citoyen.oceco.notificationPush;
    if (notificationPush) {
      const notId = Math.round(new Date().getTime() / 1000);
      Push.send({
        from: 'push',
        title,
        text,
        // payload: payloadStringify,
        sound: 'default',
        query,
        badge,
        apn: {
          sound: 'default',
        },
        contentAvailable: 1,
        androidChannel: 'PushPluginChannel',
        notId,
      });
    }
  });
};

const pushUserToken = (title, text, query, badge) => {
  const citoyensListEmail = Citoyens.find({ _id: new Mongo.ObjectID(query.userId) }, { fields: { oceco: 1 } });
  citoyensListEmail.forEach((citoyen) => {
    const notificationPush = (citoyen && !citoyen.oceco) ? true : citoyen.oceco.notificationPush;
    if (notificationPush) {
      const notId = Math.round(new Date().getTime() / 1000);
      const queryToken = {
        $and: [
          { userId: query.userId },
          { 'token.fcm': { $exists: true } },
          { enabled: { $ne: false } },
        ],
      };
      Push.appCollection.find(queryToken).forEach(function (app) {
        if (Push.debug) {
          // eslint-disable-next-line no-console
          console.log('send to token', app.token);
        }
        if (app.token.fcm) {
          Push.sendFCM(app.token.fcm, {
            from: 'push',
            title,
            text,
            sound: 'default',
            query,
            badge,
            apn: {
              sound: 'default',
            },
            contentAvailable: 1,
            androidChannel: 'PushPluginChannel',
            notId,
          });
        }
      });
    }
  });
};

const pushEmail = (title, text, payload, query) => {
  const citoyensListEmail = Citoyens.find({ _id: new Mongo.ObjectID(query.userId) }, { fields: { email: 1, name: 1, oceco: 1 } });
  const emailTpl = Assets.getText('mjml/notification.mjml');
  citoyensListEmail.forEach((citoyen) => {
    const notificationEmail = (citoyen && !citoyen.oceco) ? false : citoyen.oceco.notificationEmail;
    if (notificationEmail) {
      // console.log(citoyen.email);
      if ((Meteor.isProduction && citoyen.email) || (Meteor.isDevelopment && (citoyen.email === 'thomas.craipeau@gmail.com'))) {
        // eslint-disable-next-line no-undef
        const language = userLangageObs(citoyen._id._str);
        const lang = language || 'en';
        const email = new MJML(emailTpl);

        email.helpers({
          message: text,
          name: citoyen.name,
          userId: citoyen._id._str,
          signature: payload.target.name,
          subject: title,
          scope: lang ? i18n.__('notifications', { _locale: lang }) : i18n.__('notifications'),
          scopeName: lang ? i18n.__('View notifications', { _locale: lang }) : i18n.__('View notifications'),
          scopeUrl: Meteor.absoluteUrl('notifications'),
          ocecoUrl: Meteor.absoluteUrl(),
          Welcome: lang ? i18n.__('Welcome', { _locale: lang }) : i18n.__('Welcome'),
          Name: lang ? i18n.__('name', { _locale: lang }) : i18n.__('name'),
          Type: lang ? i18n.__('Type', { _locale: lang }) : i18n.__('Type'),
          disable_notification_email_info: lang ? i18n.__('disable_notification_email_info', { ocecoUrl: Meteor.absoluteUrl(), userId: citoyen._id._str, _locale: lang }) : i18n.__('disable_notification_email_info', { ocecoUrl: Meteor.absoluteUrl(), userId: citoyen._id._str }),
        });

        const options = {};
        options.subject = `${title} - ${payload.target.name}`;

        if (Meteor.isDevelopment) {
          options.from = Meteor.settings.mailSetting.dev.from;
          options.to = Meteor.settings.mailSetting.dev.to;
        } else {
          options.from = Meteor.settings.mailSetting.prod.from;
          options.to = citoyen.email;
        }
        // Meteor.defer(() => {
        try {
          email.send(options);
        } catch (e) {
          // console.error(`Problem sending email ${logEmailId} to ${options.to}`, e);
          throw log.error(`Problem sending email notif ${title} to ${options.to}`, e);
        }
        // });
      }
    }
  });
};

const notifPermissionArray = ({ notification, linkSelect = 'follows' }) => {
  const notifsId = Object.keys(notification.notify.id).map((key) => key);
  // DIRECT
  const arrayIdsUsersObjId = notifsId.map((id) => new Mongo.ObjectID(id));
  const arrayEnvoieDirect = Citoyens.find({ _id: { $in: arrayIdsUsersObjId }, 'oceco.notificationAllOrga': true }, { fields: { _id: 1, 'oceco.notificationAllOrga': 1 } });
  const arrayIdsUsersEnvoieDirect = arrayEnvoieDirect.map((citoyen) => citoyen._id._str);

  const queryPasDirect = {};
  queryPasDirect._id = { $in: arrayIdsUsersObjId };
  queryPasDirect['oceco.notificationAllOrga'] = false;

  const fieldsSelect = `links.${linkSelect}`;

  if (notification && notification.targetProject && notification.targetProject.id) {
    // project
    // PAS DIRECT
    // links.follows
    const linkFollowId = `${fieldsSelect}.${notification.targetProject.id}`;
    queryPasDirect[linkFollowId] = { $exists: true };
  } else if (notification && notification.target && notification.target.id) {
    // orga
    // PAS DIRECT
    // links.follows
    const linkFollowId = `${fieldsSelect}.${notification.target.id}`;
    queryPasDirect[linkFollowId] = { $exists: true };
  }

  // PAS DIRECT
  // links.follows
  const arrayEnvoiePasDirect = Citoyens.find(queryPasDirect, { fields: { _id: 1, 'oceco.notificationAllOrga': 1, 'links.follows': 1 } });
  const arrayIdsUsersyEnvoiePasDirect = arrayEnvoiePasDirect.map((citoyen) => citoyen._id._str);
  const arrayIdsUsersGroup = [...arrayIdsUsersEnvoieDirect, ...arrayIdsUsersyEnvoiePasDirect];
  return arrayIdsUsersGroup;
};

Jobs.register({
  sendEmail: function (options, helpers, logEmailId) {
    // eslint-disable-next-line no-undef
    const emailTpl = Assets.getText('mjml/email.mjml');
    // eslint-disable-next-line no-undef
    const email = new MJML(emailTpl);
    email.helpers(helpers);
    try {
      email.send(options);
    } catch (e) {
      // console.error(`Problem sending email ${logEmailId} to ${options.to}`, e);
      throw log.error(`Problem sending email ${logEmailId} to ${options.to}`, e);
    }
    this.remove();
  },
  pushEmail: function (notification) {
    if (notification && notification.notify && notification.notify.id && notification.notify.displayName) {
      const title = 'notification';
      // const text = notification.notify.displayName;

      /* const notifsId = _.map(notification.notify.id, function (ids, key) {
        return key;
      }); */

      const notifsId = notifPermissionArray({ notification });

      // verifier que présent dans Meteor.users
      const notifsIdMeteor = Meteor.users.find({ _id: { $in: notifsId } }, { fields: { _id: 1 } }).map((user) => user._id);
      // console.log(notifsIdMeteor);
      if (notifsIdMeteor && notifsIdMeteor.length > 0) {
        _.each(notifsIdMeteor, function (value) {
          const query = {};
          query.userId = value;
          const language = userLangageObs(value);
          const text = language ? notifyDisplay(notification.notify, language) : notifyDisplay(notification.notify, 'en');
          const textTarget = `${text} - ${notification.target.name}`;
          const payload = JSON.parse(JSON.stringify(notification));
          // console.log({ value, badge });
          // console.log(payload);
          pushEmail(title, textTarget, payload, query);
        }, title, notification);
      }
    }
    this.remove();
  },
  pushMobile: function (notification) {
    if (notification && notification.notify && notification.notify.id && notification.notify.displayName) {
      const title = notification.target && notification.target.name && notification.target.type === 'organizations' ? notification.target.name : 'notification';
      // const text = notification.notify.displayName;

      /* const notifsId = _.map(notification.notify.id, function (ids, key) {
        return key;
      }); */
      const notifsId = Object.keys(notification.notify.id).map((key) => key);

      // verifier que présent dans Meteor.users
      const notifsIdMeteor = Meteor.users.find({ _id: { $in: notifsId } }, { fields: { _id: 1 } }).map((user) => user._id);
      // console.log(notifsIdMeteor);
      if (notifsIdMeteor && notifsIdMeteor.length > 0) {
        notifsIdMeteor.forEach((value) => {
          const query = {};
          query.userId = value;
          const language = userLangageObs(value);
          const text = language ? notifyDisplay(notification.notify, language) : notifyDisplay(notification.notify, 'en');
          const textTarget = `${text} - ${notification.target.name}`;
          // const payload = JSON.parse(JSON.stringify(notification));
          const badge = ActivityStream.api.queryUnseen(value).count();
          // console.log({ value, badge });
          // console.log(payload);
          pushUser(title, textTarget, query, badge);
        }, title, notification);

        /* _.each(notifsIdMeteor, function (value) {
          const query = {};
          query.userId = value;
          const lang = Meteor.users.findOne({ _id: value }, { fields: { 'profile.language': 1 } });
          const text = lang && lang.profile.language ? notifyDisplay(notification.notify, lang.profile.language) : notifyDisplay(notification.notify, 'en');
          const textTarget = `${text} - ${notification.target.name}`;
          const payload = JSON.parse(JSON.stringify(notification));
          const badge = ActivityStream.api.queryUnseen(value).count();
          // console.log({ value, badge });
          // console.log(payload);
          pushUser(title, textTarget, query, badge);
        }, title, notification); */
      }
    }
    this.remove();
  },
  pushMobileUserToken: function (title, textTarget, query, badge) {
    pushUserToken(title, textTarget, query, badge);
    this.remove();
  },
  pushTimeAction: function (actionId, typeNotif) {
    const actionOne = Actions.findOne({ _id: new Mongo.ObjectID(actionId), status: 'todo' }, {
      fields: {
        _id: 1, name: 1, links: 1, parentType: 1, parentId: 1, idParentRoom: 1, status: 1,
      },
    });
    if (actionOne) {
      const notif = {};
      notif.object = {
        id: actionOne._id._str, name: actionOne.name, type: 'actions', links: actionOne.links, parentType: actionOne.parentType, parentId: actionOne.parentId, idParentRoom: actionOne.idParentRoom,
      };
      ActivityStream.api.add(notif, typeNotif, 'isActionMembers');
    }
    this.remove();
  },
});
