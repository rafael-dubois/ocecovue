/* eslint-disable meteor/no-session */
/* eslint-disable no-undef */
import Vue from 'vue';
import VueRouter from 'vue-router';
import { Meteor } from 'meteor/meteor';
import isUser from '../../ui/login/helpers/checkAuth';
import isAdmin from '../../ui/login/helpers/checkAdmin';

Vue.use(VueRouter);

// Router.onBeforeAction(ensureUpdateStore);
// Router.onBeforeAction(ensurePixelSignin, { except: ['login', 'signin', 'passwordLost', 'pageNotFound'] });
// Router.onBeforeAction(ensurePixelIsAdmin, { only: ['adminDashboard', 'newAction', 'listMembers', 'assignUserDashboard', 'pageNotFound'] });
// Router.onBeforeAction(ensurePixelSwitch, { except: ['login', 'signin', 'passwordLost', 'switch', 'switchRedirect', 'searchGlobal', 'pageNotFound'] });

const routes = [
  {
    path: '/',
    redirect: '/login',
  },
  {
    path: '/login',
    name: 'login',
    // meta: { layout: 'no-sidebar' },
    component: () => import('../../ui/login/components/Login.vue'),
    props: {},
  },
  {
    path: '/signin',
    name: 'signin',
    // meta: { layout: 'no-sidebar' },
    component: () => import('../../ui/login/components/Signin.vue'),
    props: {},
  },
  {
    path: '/password-lost',
    name: 'passwordlost',
    // meta: { layout: 'no-sidebar' },
    component: () => import('../../ui/login/components/PasswordLost.vue'),
  },
  {
    path: '/sign-out',
    name: 'signOut',
    props: {
      onBeforeAction() {
        if (Meteor.userId()) {
          Meteor.logout();
          this.redirect('home');
        }
        this.next();
        this.$router.push('/');
      },
    },
  },
  {
    path: '/home',
    name: 'homeView',
    meta: { layout: 'layoutGlobal' },
    component: () => import('../../ui/home/HomeView.vue'),
    beforeEnter: (to, from, next) => {
      isUser().then((response) => {
        // eslint-disable-next-line no-unused-expressions
        response ? next() : next({ name: 'login' });
      });
    },
  },
  {
    path: '/admin',
    name: 'admin',
    meta: { layout: 'layoutGlobal' },
    // component: Admin,
    // component: () => import('../../ui/home/AdminTest.vue'),
    // redirect: { name: 'admin-dashboard' },
    beforeEnter: isAdmin,
  },
  {
    path: '/about',
    name: 'about',
  },
  {
    path: '/switch:_id',
    name: 'switch',
    meta: { layout: 'layoutGlobal' },
    props: {
      onBeforeAction() {
        const id = this.params._id;
        Session.setPersistent('orgaCibleId', id);
        this.redirect('home');
      },
    },
  },
  {
    path: '*',
    name: 'not-found',
    component: () => import('../../ui/notfound/NotFound.vue'),
  },
];

const router = new VueRouter({
  routes,
});

export default router;
