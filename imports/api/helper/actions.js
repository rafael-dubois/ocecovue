/* eslint-disable meteor/no-session */
/* global Session */
import { Meteor } from 'meteor/meteor';
import { Mongo } from 'meteor/mongo';
import { moment } from 'meteor/momentjs:moment';
import { _ } from 'meteor/underscore';

// schemas
import { Actions } from '../collection/actions.js';
// eslint-disable-next-line no-unused-vars
import { Events } from '../collection/events.js';
// eslint-disable-next-line no-unused-vars
import { Projects } from '../collection/projects.js';
// eslint-disable-next-line no-unused-vars
import { Organizations } from '../collection/organizations.js';
import { Documents } from '../collection/documents.js';
import { Citoyens } from '../collection/citoyens.js';
import { Comments } from '../collection/comments.js';
import { Answers } from '../collection/answers.js';
import {
  queryLink, queryLinkToBeValidated, queryOptions, arrayLinkProper, arrayLinkProperNoObject, nameToCollection,
} from '../helpers.js';

if (Meteor.isClient) {
  import { Chronos } from '../client/chronos.js';

  Actions.helpers({

    isStartDate() {
      if (this.startDate) {
        const start = moment(this.startDate).toDate();
        return Chronos.moment(start).isBefore(); // True
      }
      return false;
    },
    isNotStartDate() {
      if (this.startDate) {
        const start = moment(this.startDate).toDate();
        return Chronos.moment().isBefore(start); // True
      }
      return false;
    },
    isEndDate() {
      if (this.endDate) {
        const end = moment(this.endDate).toDate();
        return Chronos.moment(end).isBefore(); // True
      }
      return false;
    },
    isNotEndDate() {
      if (this.endDate) {
        const end = moment(this.endDate).toDate();
        return Chronos.moment().isBefore(end); // True
      }
      return false;
    },
    timeSpentStart() {
      if (this.startDate) {
        return Chronos.moment(this.startDate).fromNow();
      }
      return false;
    },
    timeSpentEnd() {
      if (this.endDate) {
        return Chronos.moment(this.endDate).fromNow();
      }
      return false;
    },
    isDepense() {
      if (Session.get('settingOceco') && Session.get('settingOceco').spendNegative === true && ((Session.get('settingOceco').spendNegativeMax - this.userCredit()) * -1) >= (this.credits * -1)) {
        return true;
      }
      return this.credits <= 0 && this.userCredit() && (this.userCredit() + this.credits) >= 0;
    },
    isCreditNeg() {
      return this.credits && this.credits < 0;
    },
    podomoroSpentStart() {
      if (this.podomoroData().startedAt) {
        return Chronos.moment(this.podomoroData().startedAt).fromNow();
      }
      return false;
    },
    statusPodomoroActual() {
      return this.podomoro && this.podomoro[Meteor.userId()] && this.podomoro[Meteor.userId()].statusPodomoro ? this.podomoro[Meteor.userId()].statusPodomoro : null;
    },
    viewDescription() {
      return this.description || this.tags || this.urls;
      // eslint-disable-next-line comma-dangle
    }
  });
} else {
  Actions.helpers({
    isEndDate() {
      if (this.endDate) {
        const end = moment(this.endDate).toDate();
        return moment(end).isBefore(); // True
      }
      return false;
    },
    isNotEndDate() {
      if (this.endDate) {
        const end = moment(this.endDate).toDate();
        return moment().isBefore(end); // True
      }
      return false;
    },
    isDepense() {
      return this.credits <= 0 && this.userCredit() && (this.userCredit() + this.credits) >= 0;
    },
  });
}

Actions.helpers({
  photoActionsAlbums() {
    if (this.media && this.media.images) {
      const arrayId = this.media.images.map((_id) => new Mongo.ObjectID(_id));
      return Documents.find({ _id: { $in: arrayId } });
    }
    return undefined;
  },
  docActionsList() {
    if (this.mediaFile && this.mediaFile.files) {
      const arrayId = this.mediaFile.files.map((_id) => new Mongo.ObjectID(_id));
      return Documents.find({ _id: { $in: arrayId } });
    }
    return undefined;
  },
  isVisibleFields() {
    return true;
  },
  isPublicFields(field) {
    return this.preferences && this.preferences.publicFields && _.contains(this.preferences.publicFields, field);
  },
  isPrivateFields(field) {
    return this.preferences && this.preferences.privateFields && _.contains(this.preferences.privateFields, field);
  },
  scopeVar() {
    return 'actions';
  },
  scopeEdit() {
    return 'actionsEdit';
  },
  listScope() {
    return 'listActions';
  },
  parentScope() {
    const collection = nameToCollection(this.parentType);
    const parentOne = collection.findOne({ _id: new Mongo.ObjectID(this.parentId) });
    return parentOne;
  },
  creatorProfile() {
    return Citoyens.findOne({
      _id: new Mongo.ObjectID(this.creator),
    }, {
      fields: {
        name: 1,
        profilThumbImageUrl: 1,
      },
    });
  },
  isCreator() {
    return this.creator === Meteor.userId();
  },
  listMembersToBeValidated() {
    if (this.links && this.links.contributors) {
      const query = queryLinkToBeValidated(this.links.contributors);
      return Citoyens.find(query, queryOptions);
    }
    return false;
  },
  isPodomoroWorking(userId) {
    const bothUserId = (typeof userId !== 'undefined') ? userId : Meteor.userId();
    return this.podomoro && this.podomoro[bothUserId] && this.podomoro[bothUserId].statusPodomoro && this.podomoro[bothUserId].statusPodomoro === 'working';
  },
  podomoroData(userId) {
    const bothUserId = (typeof userId !== 'undefined') ? userId : Meteor.userId();
    return this.podomoro && this.podomoro[bothUserId] ? this.podomoro[bothUserId] : null;
  },
  isContributors(userId) {
    const bothUserId = (typeof userId !== 'undefined') ? userId : Meteor.userId();
    return !!((this.links && this.links.contributors && this.links.contributors[bothUserId] && this.isToBeValidated(bothUserId) && this.isIsInviting('contributors', bothUserId)));
  },
  userNotTerminated(userId) {
    const bothUserId = (typeof userId !== 'undefined') ? userId : Meteor.userId();
    return this.isContributors(bothUserId) && !((this.finishedBy && this.finishedBy[bothUserId]));
  },
  userIsValidated(userId) {
    if (this.finishedBy && this.finishedBy[userId] && this.finishedBy[userId] === 'validated') {
      return true;
    }
    return false;
  },
  userIsNoValidated(userId) {
    if (this.finishedBy && this.finishedBy[userId] && this.finishedBy[userId] === 'novalidated') {
      return true;
    }
    return false;
  },
  userTovalidate(userId) {
    if (this.finishedBy && this.finishedBy[userId] && this.finishedBy[userId] === 'toModerate') {
      return true;
    }
    return false;
  },
  avatarOneUserAction() {
    // si une action à un participant min et max et qu'un user à pris la tache
    // avoir son avatar sur l'action
    if (this.max === 1 && this.links && this.links.contributors) {
      const query = queryLink(this.links.contributors);
      const citoyenOne = Citoyens.findOne(query);
      if (citoyenOne && citoyenOne.profilThumbImageUrl) {
        return citoyenOne.profilThumbImageUrl;
      }
      return false;
    }
    return false;
  },
  contributorsPlusOne() {
    if (this.links && this.links.contributors) {
      const arrayContributors = arrayLinkProperNoObject(this.links.contributors);
      return arrayContributors && arrayContributors.length > 1;
    }
    return false;
  },
  contributorsOne() {
    if (this.links && this.links.contributors) {
      const arrayContributors = arrayLinkProperNoObject(this.links.contributors);
      return arrayContributors && arrayContributors.length === 1;
    }
    return false;
  },
  listContributors(search) {
    if (this.links && this.links.contributors) {
      const query = queryLink(this.links.contributors, search);
      return Citoyens.find(query, queryOptions);
    }
    return false;
  },
  listContributorsUsername(search) {
    if (this.links && this.links.contributors) {
      const query = queryLink(this.links.contributors, search);
      // console.log(Citoyens.find(query, { fields: { username: 1 } }).count());
      return Citoyens.find(query, { fields: { username: 1 } });
    }
    return false;
  },
  userCredit() {
    const citoyenOne = Citoyens.findOne({
      _id: new Mongo.ObjectID(Meteor.userId()),
    });
    return citoyenOne && citoyenOne.userCredit();
  },
  isActionDepense() {
    return Math.sign(this.credits) === -1;
  },
  isAFaire() {
    return this.credits > 0 || (this.options && this.options.creditAddPorteur);
  },
  isCreditAddPorteur() {
    return !this.credits && this.options && this.options.creditAddPorteur;
  },
  isPossibleStartActionBeforeStartDate() {
    return this.options && this.options.possibleStartActionBeforeStartDate;
  },
  isNotMax() {
    return this.max ? (this.max > this.countContributors()) : true;
  },
  projectDayHour() {
    // return moment(this.startDate).format(' ddd Do MMM à HH:mm ');
    return moment(this.startDate).format(' ddd D MMM LT ');
  },
  projectDay() {
    return moment(this.startDate).format(' ddd D MMM ');
  },
  projectDayHourEnd() {
    if (this.projectDay() === this.projectDayEnd()) {
      return moment(this.endDate).format('LT');
    }
    return moment(this.endDate).format('LLLL');
  },
  projectDayEnd() {
    return moment(this.endDate).format(' ddd D MMM ');
  },
  projectDuration() {
    const startDate = moment(this.startDate);
    const endDate = moment(this.endDate);
    return Math.round(endDate.diff(startDate, 'minutes') / 60);
  },
  actionStartFromEnd() {
    const startDate = moment(this.startDate);
    const endDate = moment(this.endDate);
    return startDate.from(endDate, true);
  },
  actionParticipantsNbr() {
    if (this.links) {
      const numberParticipant = arrayLinkProper(this.links.contributors).length;
      return numberParticipant;
    }
    return 'aucun';
  },
  creditPositive() {
    if (this.credits >= 0 || this.isCreditAddPorteur()) {
      return true;
    }
    return false;
  },
  creditNegative() {
    return -this.credits;
  },
  countContributors(search) {
    // return this.links && this.links.contributors && _.size(this.links.contributors);
    return this.listContributors(search) && this.listContributors(search).count() ? this.listContributors(search).count() : 0;
  },
  countContributorsArray() {
    if (this.links && this.links.contributors) {
      const arrayContributors = arrayLinkProperNoObject(this.links.contributors);
      return arrayContributors && arrayContributors.length > 0 ? arrayContributors.length : 0;
    }
    return 0;
  },
  isNotMin() {
    return this.min ? (this.min > this.countContributorsArray()) : true;
  },
  creditPartage() {
    if (this.options && this.options.creditSharePorteur) {
      if (this.countContributors() > 0) {
        return Math.round(this.credits / this.countContributors());
      }
    }
    return this.credits;
  },
  isToBeValidated(userId) {
    const bothUserId = (typeof userId !== 'undefined') ? userId : Meteor.userId();
    return !((this.links && this.links.contributors && this.links.contributors[bothUserId] && this.links.contributors[bothUserId].toBeValidated));
  },
  isIsInviting(scope, scopeId) {
    return !((this.links && this.links[scope] && this.links[scope][scopeId] && this.links[scope][scopeId].isInviting));
  },
  momentStartDate() {
    return moment(this.startDate).toDate();
  },
  momentEndDate() {
    return moment(this.endDate).toDate();
  },
  formatStartDate() {
    // todo : utilisé local
    // return moment(this.startDate).format('DD/MM/YYYY HH:mm');
    return moment(this.startDate).format('L LT');
  },
  formatEndDate() {
    // todo : utilisé local
    // return moment(this.endDate).format('DD/MM/YYYY HH:mm');
    return moment(this.endDate).format('L LT');
  },
  formatMilestoneStartDate() {
    return this.milestone && this.milestone.startDate && moment(this.milestone.startDate).format('L');
  },
  formatMilestoneEndDate() {
    return this.milestone && this.milestone.endDate && moment(this.milestone.endDate).format('L');
  },
  listComments() {
    // console.log('listComments');
    return Comments.find({
      contextId: this._id._str,
    }, { sort: { created: -1 } });
  },
  commentsCount() {
    if (this.commentCount) {
      return this.commentCount;
    }
    return 0;
  },
  pomodoroTotalTime() {
    if (this.podomoro && Object.keys(this.podomoro).length > 0) {
      const totalAllTime = Object.keys(this.podomoro).reduce((sum, key) => sum + parseFloat(this.podomoro[key].totalTime || 0), 0);
      // return totalAllTime;
      // return moment().duration(totalAllTime, 'minutes').format('h:mm');
      // return moment.duration({ minutes: totalAllTime }).humanize();
      return moment.utc(moment.duration(totalAllTime, 'seconds').asMilliseconds()).format('LT'); // HH:mm
    }
    return 0;
  },
  pomodoroUserTotalTime(userId) {
    const bothUserId = (typeof userId !== 'undefined') ? userId : Meteor.userId();
    if (this.podomoro && this.podomoro[bothUserId] && this.podomoro[bothUserId].totalTime) {
      // return this.podomoro[bothUserId].totalTime;
      // return moment().duration(this.podomoro[bothUserId].totalTime, 'minutes').format('h:mm');
      return moment.utc(moment.duration(this.podomoro[bothUserId].totalTime, 'seconds').asMilliseconds()).format('LTS'); // HH:mm:ss
    }
    return 0;
  },
  totalTask() {
    const totalTask = this.tasks && this.tasks.length > 0 ? this.tasks.length : 0;
    return totalTask;
  },
  totalChecked() {
    const totalChecked = this.tasks && this.tasks.length > 0 && this.tasks.filter((k) => k.checked === true).length ? this.tasks.filter((k) => k.checked === true).length : 0;
    return totalChecked;
  },
  totalUnChecked() {
    const totalUnChecked = this.tasks && this.tasks.length > 0 && this.tasks.filter((k) => k.checked === false).length ? this.tasks.filter((k) => k.checked === false).length : 0;
    return totalUnChecked;
  },
  pourTaskChecked() {
    const totalTask = this.tasks && this.tasks.length > 0 ? this.tasks.length : 0;
    const totalChecked = this.tasks && this.tasks.length > 0 && this.tasks.filter((k) => k.checked === true).length ? this.tasks.filter((k) => k.checked === true).length : 0;
    if (totalTask === 0) {
      return 0;
    }
    const pourcentage = (100 * totalChecked) / totalTask;
    if (Number.isInteger(pourcentage)) {
      return pourcentage;
    }
    return parseFloat(pourcentage).toFixed(2);
  },
  answer() {
    if (this.answerId) {
      return Answers.findOne({ _id: new Mongo.ObjectID(this.answerId) });
    }
  },
});
