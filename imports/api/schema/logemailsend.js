import SimpleSchema from 'simpl-schema';
import { Tracker } from 'meteor/tracker';

import { LogEmailOceco } from '../collection/logemailsend.js';

// eslint-disable-next-line import/prefer-default-export
export const SchemasMessagesRest = new SimpleSchema({
  parentType: { type: String, allowedValues: ['projects', 'organizations', 'events'] },
  parentId: { type: String },
  actionId: { type: String, optional: true },
  subject: { type: String },
  text: { type: String },
}, {
  tracker: Tracker,
  clean: {
    filter: true,
    autoConvert: true,
    removeEmptyStrings: true,
    trimStrings: true,
    getAutoValues: true,
    removeNullsFromArrays: true,
  },
});

const SchemasLogEmailOceco = new SimpleSchema({
  parentType: { type: String, allowedValues: ['projects', 'organizations', 'events'] },
  parentId: { type: String },
  actionId: { type: String, optional: true },
  subject: { type: String },
  text: { type: String },
  userId: { type: String },
  createdAt: {
    type: Date,
    autoValue() {
      if (this.isInsert) {
        return new Date();
      } if (this.isUpsert) {
        return { $setOnInsert: new Date() };
      }
      this.unset();
    },
  },
}, {
  clean: {
    filter: true,
    autoConvert: true,
    removeEmptyStrings: true,
    trimStrings: true,
    getAutoValues: true,
    removeNullsFromArrays: true,
  },
});

LogEmailOceco.attachSchema(SchemasLogEmailOceco);
