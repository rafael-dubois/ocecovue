import SimpleSchema from 'simpl-schema';
import { Tracker } from 'meteor/tracker';

// schemas
import { baseSchema, SchemasRolesRest } from './schema.js';

// eslint-disable-next-line import/prefer-default-export
export const SchemasRoomsRest = new SimpleSchema(baseSchema.pick('description', 'name'), {
  tracker: Tracker,
});
SchemasRoomsRest.extend(SchemasRolesRest.pick('roles', 'roles.$'));
SchemasRoomsRest.extend({
  parentId: {
    type: String,
  },
  parentType: {
    type: String,
    allowedValues: ['projects', 'organizations', 'events'],
  },
  /* status: {
    type: String,
    allowedValues: ['open', 'closed'],
  }, */
});
