import SimpleSchema from 'simpl-schema';
import { Tracker } from 'meteor/tracker';

// eslint-disable-next-line import/prefer-default-export
export const SchemasFormsRest = new SimpleSchema({
  group: {
    type: Array,
  },
  'group.$': {
    type: String,
  },
  nature: {
    type: Array,
  },
  'nature.$': {
    type: String,
  },
  parentId: {
    type: String,
  },
  parentType: {
    type: String,
    allowedValues: ['forms'],
  },
}, {
  tracker: Tracker,
  clean: {
    filter: true,
    autoConvert: true,
    removeEmptyStrings: true,
    trimStrings: true,
    getAutoValues: true,
    removeNullsFromArrays: true,
  },
});
