import SimpleSchema from 'simpl-schema';
import { Tracker } from 'meteor/tracker';

export const SchemasLogUserActionsRest = new SimpleSchema({
  userId: {
    type: String,
  },
  organizationId: {
    type: String,
  },
  actionId: {
    type: String,
    optional: true,
  },
  credits: {
    type: SimpleSchema.Integer,
  },
  commentaire: {
    type: String,
  },
}, {
  tracker: Tracker,
  clean: {
    filter: true,
    autoConvert: true,
    removeEmptyStrings: true,
    trimStrings: true,
    getAutoValues: true,
    removeNullsFromArrays: true,
  },
});

export const SchemasValidateUserActionsRest = new SimpleSchema({
  userId: {
    type: String,
  },
  organizationId: {
    type: String,
  },
  actionId: {
    type: String,
  },
  credits: {
    type: SimpleSchema.Integer,
    min: 0,
  },
  commentaire: {
    type: String,
  },
}, {
  tracker: Tracker,
  clean: {
    filter: true,
    autoConvert: true,
    removeEmptyStrings: true,
    trimStrings: true,
    getAutoValues: true,
    removeNullsFromArrays: true,
  },
});

export const SchemasTibilletRest = new SimpleSchema({
  organizationId: {
    type: String,
  },
  credits: {
    type: SimpleSchema.Integer,
    min: 1,
  },
  numberCarte: {
    type: String,
    min: 8,
    max: 8,
  },
}, {
  tracker: Tracker,
  clean: {
    filter: true,
    autoConvert: true,
    removeEmptyStrings: true,
    trimStrings: true,
    getAutoValues: true,
    removeNullsFromArrays: true,
  },
});
