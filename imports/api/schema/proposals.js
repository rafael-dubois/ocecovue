import { moment } from 'meteor/momentjs:moment';
import SimpleSchema from 'simpl-schema';
import { Tracker } from 'meteor/tracker';

// schemas
import { baseSchema, blockBaseSchema } from './schema.js';

// SimpleSchema.debug = true;

export const SchemasProposalsRest = new SimpleSchema(baseSchema.pick('description', 'tags', 'tags.$'), {
  tracker: Tracker,
});
SchemasProposalsRest.extend({
  idParentRoom: {
    type: String,
  },
  title: {
    type: String,
  },
  arguments: {
    type: String,
    optional: true,
  },
  amendementActivated: {
    type: Boolean,
  },
  amendementDateEnd: {
    type: Date,
    optional: true,
    // eslint-disable-next-line consistent-return
    custom() {
      if (this.field('amendementActivated').value === true && !this.isSet && (!this.operator || (this.value === null || this.value === ''))) {
        return 'required';
      }
      // amendementDateEnd plus petit que voteDateEnd
      const amendementDateEnd = moment(this.value).toDate();
      const voteDateEnd = moment(this.field('voteDateEnd').value).toDate();
      if (moment(voteDateEnd).isBefore(amendementDateEnd)) {
        return 'maxDateAmendment';
      }
    },
  },
  voteActivated: {
    type: Boolean,
    defaultValue: true,
  },
  voteDateEnd: {
    type: Date,
    // eslint-disable-next-line consistent-return
    custom() {
      // voteDateEnd plus grand que amendementDateEnd
      if (this.field('amendementActivated').value === true && this.field('amendementDateEnd').value) {
        const amendementDateEnd = moment(this.field('amendementDateEnd').value).toDate();
        const voteDateEnd = moment(this.value).toDate();
        if (moment(voteDateEnd).isBefore(amendementDateEnd)) {
          return 'minDateVote';
        }
      }
    },
  },
  majority: {
    type: SimpleSchema.Integer,
    defaultValue: 50,
    min: 50,
    max: 100,
  },
  voteAnonymous: {
    type: Boolean,
    defaultValue: true,
  },
  voteCanChange: {
    type: Boolean,
    defaultValue: true,
  },
  parentId: {
    type: String,
  },
  parentType: {
    type: String,
    allowedValues: ['projects', 'organizations', 'events'],
  },
  status: {
    type: String,
    allowedValues: ['done', 'disabled', 'amendable', 'tovote'],
  },
  urls: {
    type: Array,
    optional: true,
  },
  'urls.$': {
    type: String,
  },
});

export const BlockProposalsRest = new SimpleSchema(blockBaseSchema, {
  tracker: Tracker,
});
SchemasProposalsRest.extend({
  id: {
    type: String,
  },
  txtAmdt: {
    type: String,
  },
  typeAmdt: {
    type: String,
  },
});
