import SimpleSchema from 'simpl-schema';
import { Tracker } from 'meteor/tracker';

export const SchemasCommentsRest = new SimpleSchema({
  text: {
    type: String,
  },
  parentCommentId: {
    type: String,
    optional: true,
  },
  argval: {
    type: String,
    optional: true,
  },
  contextId: {
    type: String,
  },
  contextType: {
    type: String,
  },
  mentions: {
    type: Array,
    optional: true,
  },
  'mentions.$': {
    type: Object,
    optional: true,
  },
  'mentions.$.id': {
    type: String,
    optional: true,
  },
  'mentions.$.name': {
    type: String,
    optional: true,
  },
  'mentions.$.avatar': {
    type: String,
    optional: true,
  },
  'mentions.$.type': {
    type: String,
    optional: true,
  },
  'mentions.$.slug': {
    type: String,
    optional: true,
  },
  'mentions.$.value': {
    type: String,
    optional: true,
  },
}, {
  tracker: Tracker,
});

export const SchemasCommentsEditRest = new SimpleSchema({
  text: {
    type: String,
  },
  parentCommentId: {
    type: String,
    optional: true,
  },
  contextId: {
    type: String,
  },
  contextType: {
    type: String,
  },
  mentions: {
    type: Array,
    optional: true,
  },
  'mentions.$': {
    type: Object,
    optional: true,
  },
  'mentions.$.id': {
    type: String,
    optional: true,
  },
  'mentions.$.name': {
    type: String,
    optional: true,
  },
  'mentions.$.avatar': {
    type: String,
    optional: true,
  },
  'mentions.$.type': {
    type: String,
    optional: true,
  },
  'mentions.$.slug': {
    type: String,
    optional: true,
  },
  'mentions.$.value': {
    type: String,
    optional: true,
  },
}, {
  tracker: Tracker,
});
