import { Meteor } from 'meteor/meteor';
import { Mongo } from 'meteor/mongo';
import { version } from './version.js';

// eslint-disable-next-line import/prefer-default-export
export const AppUpdatesCollection = new Mongo.Collection('ocecoapppdates', { idGeneration: 'MONGO' });

/*
appUpdate.ignoreVersions
title: appUpdateData.title || 'Atualização disponível',
content: appUpdateData.message || message,
confirmText: appUpdateData.actionLabel || 'Beleza',
cancelText: appUpdateData.noActionLabel || 'Mais tarde',
hideCancel: !!appUpdateData.forceUpdate,
*/

Meteor.startup(() => {
  if (AppUpdatesCollection.find({}).count() === 0) {
    AppUpdatesCollection.insert({
      title: 'Mise à jour', message: 'Vous devez mettre à jour l\'application depuis les stores', actionLabel: '', noActionLabel: '', forceUpdate: false, ignoreVersions: ['135'],
    });
  }
});

Meteor.methods({
  getAppUpdateData({ clientVersion } = {}) {
    this.unblock();

    if (Meteor.isClient) return null;

    const appUpdate = AppUpdatesCollection.findOne() || {};

    const result = {
      ...appUpdate,
      ...(appUpdate.ignoreVersions
        && appUpdate.ignoreVersions.length
        && appUpdate.ignoreVersions.includes(clientVersion)
        ? { ignore: true }
        : {}),
      version,
    };

    // eslint-disable-next-line no-console
    /* console.log({
      message: `getAppUpdateData clientVersion=${clientVersion}, newClientVersion=${version}, ${JSON.stringify(
        result,
      )}`,
      appUpdateData: appUpdate,
      appUpdateResult: result,
      clientVersion,
    }); */
    return result;
  },
});
