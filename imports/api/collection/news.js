import { Meteor } from 'meteor/meteor';
import { Mongo } from 'meteor/mongo';

// eslint-disable-next-line import/prefer-default-export
export const News = new Mongo.Collection('news', { idGeneration: 'MONGO' });

if (Meteor.isServer) {
  // Index
  /*  News.rawCollection().createIndex(
      { 'target.id': 1 },
      { name: 'target_id', partialFilterExpression: { 'target.id': { $exists: true } }, background: true }
      , (e) => {
        if (e) {
          // console.log(e);
        }
      });

    News.rawCollection().createIndex(
      { 'mentions.id': 1 },
      { name: 'mentions_id', partialFilterExpression: { 'mentions.id': { $exists: true } }, background: true }
      , (e) => {
        if (e) {
          // console.log(e);
        }
      });

    News.rawCollection().createIndex(
      { 'target.id': 1, 'scope.type': 1 },
      { name: 'target_id_scope', partialFilterExpression: { 'target.id': { $exists: true }, 'scope.type': { $exists: true } }, background: true }
      , (e) => {
        if (e) {
          // console.log(e);
        }
      });

    News.rawCollection().createIndex(
      { author: 1 },
      { name: 'author_id', partialFilterExpression: { author: { $exists: true } }, background: true }
      , (e) => {
        if (e) {
          // console.log(e);
        }
      });

    News.rawCollection().createIndex(
      { author: 1, 'target.id': 1 },
      { name: 'author_target_id', partialFilterExpression: { author: { $exists: true }, 'target.id': { $exists: true } }, background: true }
      , (e) => {
        if (e) {
          // console.log(e);
        }
      });

   News.rawCollection().createIndex(
      { author: 1, type: 1, 'scope.type' : 1 },
      { name: 'author_targetIsAuthor_exists', partialFilterExpression: { author: { $exists: true },type: { $exists: true },'scope.type': { $exists: true },targetIsAuthor: { $exists: false } }, background: true }
    , (e) => {
      if(e){
        console.log(e)
      }
  }); */
}
