import { Mongo } from 'meteor/mongo';

// eslint-disable-next-line import/prefer-default-export
export const Resolutions = new Mongo.Collection('resolutions', { idGeneration: 'MONGO' });
