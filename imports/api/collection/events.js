import { Mongo } from 'meteor/mongo';

// eslint-disable-next-line import/prefer-default-export
export const Events = new Mongo.Collection('events', { idGeneration: 'MONGO' });
