import { Mongo } from 'meteor/mongo';

// eslint-disable-next-line import/prefer-default-export
export const Rooms = new Mongo.Collection('rooms', { idGeneration: 'MONGO' });
