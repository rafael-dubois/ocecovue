import { Mongo } from 'meteor/mongo';

// eslint-disable-next-line import/prefer-default-export
export const Proposals = new Mongo.Collection('proposals', { idGeneration: 'MONGO' });
