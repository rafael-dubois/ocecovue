/* eslint-disable meteor/no-session */
/* global Session IonPopup IonActionSheet IonToast */
import { Meteor } from 'meteor/meteor';
import { Template } from 'meteor/templating';
import { Router } from 'meteor/iron:router';
import { Counter } from 'meteor/natestrauser:publish-performant-counts';
import { Mongo } from 'meteor/mongo';
import i18n from 'meteor/universe:i18n';

import './card.html';
import { lazy } from '../iolazyload.js';
import { Citoyens } from '../../../api/collection/citoyens.js';
import { Organizations } from '../../../api/collection/organizations.js';

import { pageSession, searchAction } from '../../../api/client/reactive.js';

Template.scopeCard.onRendered(function () {
  lazy.lazyLoad();
  this.autorun(function () {
    if (Template.parentData().profilThumbImageUrl) {
      lazy.lazyLoad();
    }
  });
});

Template.scopeCard.helpers({
  preferenceTrue(value) {
    return !!((value === true || value === 'true'));
  },
  listMembers() {
    // eslint-disable-next-line meteor/no-session
    return Organizations.findOne({ _id: new Mongo.ObjectID(Session.get('orgaCibleId')) }).membersPopMap();
  },
});

Template.scopeBoardActions.onCreated(function () {
  this.autorun(function () {
    const dataContext = Template.currentData();
    Meteor.subscribe('scopeActionIndicatorCount', dataContext.scope, dataContext._id, 'all');
  });
});

Template.scopeBoardActions.helpers({
  countActionsStatus(status) {
    const dataContext = Template.currentData();
    return Counter.get(`countScopeAction.${dataContext._id}.${dataContext.scope}.${status}`);
  },
});

Template.scopeBoardActionsItem.onCreated(function () {
  this.autorun(function () {
    const dataContext = Template.currentData();
    Meteor.subscribe('scopeActionIndicatorCount', dataContext.scope, dataContext._id, dataContext.status, dataContext.milestoneId);
  });
});

Template.scopeBoardActionsItem.helpers({
  countActionsStatus(status) {
    const dataContext = Template.currentData();
    const countName = dataContext.milestoneId ? `countScopeAction.${dataContext._id}.${dataContext.scope}.${status}.${dataContext.milestoneId}` : `countScopeAction.${dataContext._id}.${dataContext.scope}.${status}`;
    return Counter.get(countName);
  },
  pourActionsStatus(totalStatus, status) {
    const dataContext = Template.currentData();
    const countName = dataContext.milestoneId ? `countScopeAction.${dataContext._id}.${dataContext.scope}.${status}.${dataContext.milestoneId}` : `countScopeAction.${dataContext._id}.${dataContext.scope}.${status}`;
    const countTodo = dataContext.milestoneId ? `countScopeAction.${dataContext._id}.${dataContext.scope}.todo.${dataContext.milestoneId}` : null;
    const countNameTotalStatus = dataContext.milestoneId ? `countScopeAction.${dataContext._id}.${dataContext.scope}.${totalStatus}.${dataContext.milestoneId}` : `countScopeAction.${dataContext._id}.${dataContext.scope}.${totalStatus}`;
    const total = Counter.get(countNameTotalStatus);
    const count = Counter.get(countName);
    if (total === 0) {
      return 0;
    }
    let pourcentage = (100 * count) / total;
    if (status === 'done' && Counter.get(countTodo) === 0) {
      pourcentage = 100;
    }
    if (Number.isInteger(pourcentage)) {
      return pourcentage;
    }
    return parseFloat(pourcentage).toFixed(2);
  },
});

Template.scopeBoardUserActions.onCreated(function () {
  this.autorun(function () {
    const dataContext = Template.currentData();
    Meteor.subscribe('citoyenLight', dataContext.userId);
    Meteor.subscribe('scopeUserActionIndicatorCount', dataContext.scope, dataContext._id, 'all', dataContext.userId);
  });
});

Template.scopeBoardUserActions.helpers({
  userItem() {
    const dataContext = Template.currentData();
    return Citoyens.findOne({ _id: new Mongo.ObjectID(dataContext.userId) });
  },
  countUserActionsStatus(status, userId) {
    const dataContext = Template.currentData();
    return Counter.get(`countScopeUserAction.${userId}.${dataContext._id}.${dataContext.scope}.${status}`);
  },
});

Template.scopeBoardUserActions.events({
  'click .searchuser-js'(event) {
    event.preventDefault();
    if (this) {
      const userItem = Template.scopeBoardUserActions.__helpers.get('userItem').call();
      searchAction.set('search', `@${userItem.username}`);
      Router.go('actionsListOrga', { orgaCibleId: Session.get('orgaCibleId'), _id: this._id, scope: this.scope });
    } else {
      searchAction.set('search', null);
    }
  },
});

Template.scopeBoardUserActionsItem.onCreated(function () {
  this.autorun(function () {
    const dataContext = Template.currentData();
    Meteor.subscribe('scopeUserActionIndicatorCount', dataContext.scope, dataContext._id, dataContext.status, dataContext.userId);
  });
});

Template.scopeBoardUserActionsItem.helpers({
  countUserActionsStatus(status, userId) {
    const dataContext = Template.currentData();
    return Counter.get(`countScopeUserAction.${userId}.${dataContext._id}.${dataContext.scope}.${status}`);
  },
  pourUserActionsStatus(totalStatus, status, userId) {
    const dataContext = Template.currentData();
    const total = Counter.get(`countScopeUserAction.${userId}.${dataContext._id}.${dataContext.scope}.${totalStatus}`);
    const count = Counter.get(`countScopeUserAction.${userId}.${dataContext._id}.${dataContext.scope}.${status}`);
    if (total === 0) {
      return 0;
    }
    const pourcentage = (100 * count) / total;
    if (Number.isInteger(pourcentage)) {
      return pourcentage;
    }
    return parseFloat(pourcentage).toFixed(2);
  },
});

Template.actionSheet.events({
  'click .action-card-citoyen'(event) {
    event.preventDefault();
    // info,description,contact
    IonActionSheet.show({
      titleText: i18n.__('Actions Citoyens'),
      buttons: [
        { text: `${i18n.__('edit info')}` },
        { text: `${i18n.__('edit network')}` },
        { text: `${i18n.__('edit description')}` },
        { text: `${i18n.__('edit address')}` },
        { text: `${i18n.__('edit privacy settings')}` },
        { text: `${i18n.__('edit notification oceco')}` },
      ],
      cancelText: i18n.__('cancel'),
      cancel() {
        // console.log('Cancelled!');
      },
      buttonClicked(index) {
        if (index === 0) {
          // console.log('Edit!');
          Router.go('citoyensBlockEdit', { _id: Router.current().params._id, block: 'info' });
        }
        if (index === 1) {
          // console.log('Edit!');
          Router.go('citoyensBlockEdit', { _id: Router.current().params._id, block: 'network' });
        }
        if (index === 2) {
          // console.log('Edit!');
          Router.go('citoyensBlockEdit', { _id: Router.current().params._id, block: 'descriptions' });
        }
        if (index === 3) {
          // console.log('Edit!');
          Router.go('citoyensBlockEdit', { _id: Router.current().params._id, block: 'locality' });
        }
        if (index === 4) {
          // console.log('Edit!');
          Router.go('citoyensBlockEdit', { _id: Router.current().params._id, block: 'preferences' });
        }
        if (index === 5) {
          // console.log('Edit!');
          Router.go('ocecoCitoyenEdit', { _id: Router.current().params._id });
        }
        return true;
      },
    });
  },
  'click .action-card-events'(event) {
    event.preventDefault();
    // info,description,contact
    IonActionSheet.show({
      titleText: i18n.__('Actions Events'),
      buttons: [
        { text: `${i18n.__('edit info')}` },
        { text: `${i18n.__('edit network')}` },
        { text: `${i18n.__('edit description')}` },
        { text: `${i18n.__('edit address')}` },
        { text: `${i18n.__('edit dates')}` },
      ],
      destructiveText: `${i18n.__('delete')} <i class="icon ion-trash-a"></i>`,
      destructiveButtonClicked() {
        // console.log('Edit!');
        IonPopup.confirm({
          title: i18n.__('delete'),
          template: i18n.__('Delete this event'),
          onOk() {
            Meteor.call('deleteElementAdmin', { scope: 'events', scopeId: Router.current().params._id }, (error) => {
              if (error) {
                IonToast.show({
                  title: i18n.__('Error'), position: 'top', type: 'error', showClose: true, template: `<i class="icon fa fa-exclamation-circle"></i> ${error.reason.replace(': ', '')}`,
                });
              } else {
                Router.go('/');
              }
            });
          },
          onCancel() {
          },
          cancelText: i18n.__('no'),
          okText: i18n.__('yes'),
        });
        return true;
      },
      cancelText: i18n.__('cancel'),
      cancel() {
        // console.log('Cancelled!');
      },
      buttonClicked(index) {
        if (index === 0) {
          // console.log('Edit!');
          Router.go('eventsBlockEdit', { _id: Router.current().params._id, block: 'info' });
        }
        if (index === 1) {
          // console.log('Edit!');
          Router.go('eventsBlockEdit', { _id: Router.current().params._id, block: 'network' });
        }
        if (index === 2) {
          // console.log('Edit!');
          Router.go('eventsBlockEdit', { _id: Router.current().params._id, block: 'descriptions' });
        }
        if (index === 3) {
          // console.log('Edit!');
          Router.go('eventsBlockEdit', { _id: Router.current().params._id, block: 'locality' });
        }
        if (index === 4) {
          // console.log('Edit!');
          Router.go('eventsBlockEdit', { _id: Router.current().params._id, block: 'when' });
        }
        /* if (index === 5) {
          // console.log('Edit!');
          Router.go('eventsBlockEdit', { _id: Router.current().params._id, block: 'preferences' });
        } */
        return true;
      },
    });
  },
  'click .action-card-organizations'(event) {
    event.preventDefault();
    // info,description,contact
    IonActionSheet.show({
      titleText: i18n.__('Actions Organizations'),
      buttons: [
        { text: `${i18n.__('edit info')}` },
        { text: `${i18n.__('edit network')}` },
        { text: `${i18n.__('edit description')}` },
        { text: `${i18n.__('edit address')}` },
      ],
      cancelText: i18n.__('cancel'),
      cancel() {
        // console.log('Cancelled!');
      },
      buttonClicked(index) {
        if (index === 0) {
          // console.log('Edit!');
          Router.go('organizationsBlockEdit', { _id: Router.current().params._id, block: 'info' });
        }
        if (index === 1) {
          // console.log('Edit!');
          Router.go('organizationsBlockEdit', { _id: Router.current().params._id, block: 'network' });
        }
        if (index === 2) {
          // console.log('Edit!');
          Router.go('organizationsBlockEdit', { _id: Router.current().params._id, block: 'descriptions' });
        }
        if (index === 3) {
          // console.log('Edit!');
          Router.go('organizationsBlockEdit', { _id: Router.current().params._id, block: 'locality' });
        }
        if (index === 4) {
          // console.log('Edit!');
          Router.go('organizationsBlockEdit', { _id: Router.current().params._id, block: 'preferences' });
        }
        return true;
      },
    });
  },
  'click .action-card-projects'(event) {
    event.preventDefault();
    // info,description,contact
    const arrayButtons = [
      { text: `${i18n.__('edit info')}` },
      // { text: `${i18n.__('edit network')}` },
      { text: `${i18n.__('edit description')}` },
      // { text: `${i18n.__('edit address')}` },
      { text: `${i18n.__('edit dates')}` },
    ];
    if (Session.get('settingOceco') && Session.get('settingOceco').milestonesProject) {
      arrayButtons.push({ text: `${i18n.__('edit config oceco')}` });
    }
    IonActionSheet.show({
      titleText: i18n.__('Actions Projects'),
      buttons: arrayButtons,
      destructiveText: `${i18n.__('delete')} <i class="icon ion-trash-a"></i>`,
      destructiveButtonClicked() {
        // console.log('Edit!');
        IonPopup.confirm({
          title: i18n.__('delete'),
          template: i18n.__('Delete this project'),
          onOk() {
            Meteor.call('deleteElementAdmin', { scope: 'projects', scopeId: Router.current().params._id }, (error) => {
              if (error) {
                IonToast.show({
                  title: i18n.__('Error'), position: 'top', type: 'error', showClose: true, template: `<i class="icon fa fa-exclamation-circle"></i> ${error.reason.replace(': ', '')}`,
                });
              } else {
                IonToast.show({
                  template: i18n.__('deleted'), position: 'bottom', type: 'success', showClose: false, title: `<i class="icon ion-checkmark"></i> ${i18n.__('Project')}`,
                });
                Router.go('/');
              }
            });
          },
          onCancel() {
          },
          cancelText: i18n.__('no'),
          okText: i18n.__('yes'),
        });
        return true;
      },
      cancelText: i18n.__('cancel'),
      cancel() {
        // console.log('Cancelled!');
      },
      buttonClicked(index) {
        if (index === 0) {
          // console.log('Edit!');
          Router.go('projectsBlockEdit', { _id: Router.current().params._id, block: 'info' });
        }
        /* if (index === 1) {
          Router.go('projectsBlockEdit', { _id: Router.current().params._id, block: 'network' });
        } */
        if (index === 1) {
          // console.log('Edit!');
          Router.go('projectsBlockEdit', { _id: Router.current().params._id, block: 'descriptions' });
        }
        /* if (index === 3) {
          // console.log('Edit!');
          Router.go('projectsBlockEdit', { _id: Router.current().params._id, block: 'locality' });
        } */
        if (index === 2) {
          // console.log('Edit!');
          Router.go('projectsBlockEdit', { _id: Router.current().params._id, block: 'when' });
        }
        /* if (index === 3) {
          // console.log('Edit!');
          // Router.go('projectsBlockEdit', { _id: Router.current().params._id, block: 'preferences' });
        } */
        if (index === 3) {
          // console.log('Edit!');
          Router.go('ocecoProjectEdit', { _id: Router.current().params._id });
        }
        return true;
      },
    });
  },
});

Template.scopeFileUpload.onRendered(function () {
  const self = this;
  const template = Template.instance();

  pageSession.set('drop', false);
  pageSession.set('fileCount', 0);
  pageSession.set('fileArray', []);

  window.addEventListener('paste', (e) => {
    if (e.clipboardData.files) {
      if (e.clipboardData.files.length === 1) {
        if (e.clipboardData.files[0].type === 'image/png' || e.clipboardData.files[0].type === 'image/jpg' || e.clipboardData.files[0].type === 'image/jpge' || e.clipboardData.files[0].type === 'image/jpeg') {
          const fileInput = template.find('#file-upload');
          if (fileInput) {
            fileInput.files = e.clipboardData.files;
            // console.log(fileInput);
            pageSession.set('drop', true);
            self.$('#file-upload').trigger('change');
          }
        }
      }
    }
  });

  const dropZone = document.body;
  if (dropZone) {
    const hoverClassName = 'drop';

    dropZone.addEventListener('dragenter', function (e) {
      e.preventDefault();
      dropZone.classList.add(hoverClassName);
    });

    dropZone.addEventListener('dragover', function (e) {
      e.preventDefault();
      dropZone.classList.add(hoverClassName);
    });

    dropZone.addEventListener('dragleave', function (e) {
      e.preventDefault();
      dropZone.classList.remove(hoverClassName);
    });

    dropZone.addEventListener('drop', (e) => {
      e.preventDefault();
      dropZone.classList.remove(hoverClassName);
      const { files } = e.dataTransfer;
      if (files) {
        if (files.length === 1) {
          if (files[0].type === 'image/png' || files[0].type === 'image/jpg' || files[0].type === 'image/jpge' || files[0].type === 'image/jpeg') {
            const fileInput = template.find('#file-upload');
            fileInput.files = files;
            // console.log(fileInput);
            pageSession.set('drop', true);
            self.$('#file-upload').trigger('change');
          }
        }
      }
    });
  }
});
