class IOlazy {
  constructor({ image = '.lazyload', threshold = 0.006, rootMargin = '0px' } = {}) {
    this.threshold = threshold;
    this.rootMargin = rootMargin;
    this.dom = image;
    this.observer = new IntersectionObserver(this.handleChange.bind(this), {
      threshold: [this.threshold],
      rootMargin: this.rootMargin,
    });
  }

  handleChange(changes) {
    changes.forEach((change) => {
      if (change.isIntersecting) {
        if (change.target.tagName === 'IMG') {
          change.target.addEventListener('load', () => {
            change.target.classList.add('visible');
          });
        } else {
          change.target.classList.add('visible');
        }

        if (change.target.getAttribute('data-src')) {
          change.target.src = change.target.getAttribute('data-src');
        }

        this.observer.unobserve(change.target);
      }
    });
  }

  lazyLoad() {
    this.image = document.querySelectorAll(this.dom);
    this.image.forEach((img) => {
      this.observer.observe(img);
    });
  }
}

const lazy = new IOlazy({
  image: 'img',
  threshold: 1,
});

export { IOlazy, lazy };
