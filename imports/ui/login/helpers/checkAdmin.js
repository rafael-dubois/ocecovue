/* eslint-disable no-unused-expressions */
/* eslint-disable meteor/no-session */
import { Meteor } from 'meteor/meteor';
import { Tracker } from 'meteor/tracker';

// eslint-disable-next-line no-unused-vars
const isAdmin = () => new Promise((resolve, reject) => {
  Tracker.autorun((c) => {
    // stop computation when Meteor.user() is ready
    Meteor.user() !== undefined && c.stop();
    // return false if user is a guest
    Meteor.user() === null && resolve(false);
    // return true if user is logged in
    Meteor.user() && resolve(true);
  });
});

export default isAdmin;
