/* eslint-disable no-unused-expressions */
/* eslint-disable meteor/no-session */
/* global Session */
import { Meteor } from 'meteor/meteor';
import { Tracker } from 'meteor/tracker';

// eslint-disable-next-line no-unused-vars
const isSwitch = () => new Promise((resolve, reject) => {
  Tracker.autorun((c) => {
    // return false if user is a guest
    if (Meteor.user() === null) {
      c.stop();
      resolve(false);
    } else if (Meteor.user() && Meteor.user().profile && Meteor.user().profile.pixelhumain && !Session.get('orgaCibleId')) {
      c.stop();
      resolve(false);
    } else if (Meteor.user() && Meteor.user().profile && Meteor.user().profile.pixelhumain && Session.get('orgaCibleId')) {
      c.stop();
      resolve(true);
    }
  });
});

export default isSwitch;
