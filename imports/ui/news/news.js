/* eslint-disable no-template-curly-in-string */
/* eslint-disable no-shadow */
/* eslint-disable consistent-return */
/* eslint-disable meteor/template-names */
/* eslint-disable no-underscore-dangle */
/* eslint-disable meteor/no-session */
/* global Session IonToast IonActionSheet Platform */
import { Meteor } from 'meteor/meteor';
import { Template } from 'meteor/templating';
import { Router } from 'meteor/iron:router';
import { $ } from 'meteor/jquery';
import { _ } from 'meteor/underscore';
import { moment } from 'meteor/momentjs:moment';
import { Counter } from 'meteor/natestrauser:publish-performant-counts';
import { MeteorCameraUI } from 'meteor/aboire:camera-ui';
import { AutoForm } from 'meteor/aldeed:autoform';
import i18n from 'meteor/universe:i18n';
import { ReactiveDict } from 'meteor/reactive-dict';
import { ReactiveVar } from 'meteor/reactive-var';
import { Mongo } from 'meteor/mongo';
import { IonPopup, IonModal, IonLoading } from 'meteor/meteoric:ionic';

// import '../qrcode/qrcode.js';

// submanager
import { newsListSubs, filActusSubs } from '../../api/client/subsmanager.js';

import { Events } from '../../api/collection/events.js';
import { Organizations } from '../../api/collection/organizations.js';
import { Projects } from '../../api/collection/projects.js';
import { Citoyens } from '../../api/collection/citoyens.js';
import { Rooms } from '../../api/collection/rooms.js';
import { Actions } from '../../api/collection/actions.js';
import { Forms } from '../../api/collection/forms.js';
import { Answers } from '../../api/collection/answers.js';

import { News } from '../../api/collection/news.js';
import { SchemasNewsRestBase } from '../../api/schema/news.js';

import { nameToCollection, compareValues, searchQuerySortActived } from '../../api/helpers.js';

import { projectsCountOrga, eventsCountOrga } from '../../api/helpersOrga.js';

import { searchAction, pageSession as pageSessionAction } from '../../api/client/reactive.js';

import { lazy } from '../components/iolazyload.js';

import './news.html';

import '../components/directory/list.js';
import '../components/news/button-card.js';
import '../components/news/card.js';

window.Events = Events;
window.Organizations = Organizations;
window.Projects = Projects;
window.Citoyens = Citoyens;
window.Rooms = Rooms;
window.Forms = Forms;
window.Answers = Answers;

const pageSession = new ReactiveDict('pageNews');

Template.newsList.onCreated(function () {
  this.readyScopeDetail = new ReactiveVar();

  pageSession.setDefault('limit', 5);
  pageSession.setDefault('limitFilActus', 5);


  this.autorun(function () {
    if (Router.current().route.getName() === 'newsList') {
      pageSession.set('selectview', 'scopeNewsTemplate');
    } else if (Router.current().route.getName() === 'notificationsList' || Router.current().route.getName() === 'notificationsListOrga') {
      pageSession.set('selectview', 'scopeNotificationsTemplate');
    } else if (Router.current().route.getName() === 'actusList') {
      pageSession.set('selectview', 'scopeFilActusTemplate');
    } else if (Router.current().route.getName() === 'organizationsList') {
      pageSession.set('selectview', 'scopeOrganizationsTemplate');
    } else if (Router.current().route.getName() === 'projectsList') {
      pageSession.set('selectview', 'scopeProjectsTemplate');
    } else if (Router.current().route.getName() === 'eventsList' || Router.current().route.getName() === 'eventsListOrga') {
      if (Router.current().params.scope === 'organizations') {
        pageSession.set('selectview', 'scopeProjectsEventsTemplate');
      } else {
        pageSession.set('selectview', 'scopeEventsTemplate');
      }
    } else if (Router.current().route.getName() === 'formsList' || Router.current().route.getName() === 'formsListOrga') {
      pageSession.set('selectview', 'scopeFormsTemplate');
    } else if (Router.current().route.getName() === 'assessListOrga') {
      pageSession.set('selectview', 'scopeAssessTemplate');
    } else if (Router.current().route.getName() === 'answersList' || Router.current().route.getName() === 'answersListOrga') {
      pageSession.set('selectview', 'scopeAnswersTemplate');
    } else if (Router.current().route.getName() === 'actionsList' || Router.current().route.getName() === 'actionsListOrga') {
      pageSession.set('selectview', 'scopeActionsTemplate');
      pageSession.set('selectsubview', 'aFaire');
    } else if (Router.current().route.getName() === 'actionsListDepense' || Router.current().route.getName() === 'actionsListDepenseOrga') {
      pageSession.set('selectview', 'scopeActionsTemplate');
      pageSession.set('selectsubview', 'depenses');
    } else if (Router.current().route.getName() === 'contributorsList' || Router.current().route.getName() === 'contributorsListOrga') {
      pageSession.set('selectview', 'scopeProjectsContributorsTemplate');
    } else if (Router.current().route.getName() === 'projectsMilestonesList' || Router.current().route.getName() === 'projectsMilestonesListOrga') {
      pageSession.set('selectview', 'scopeProjectMilestonesTemplate');
    } else if (Router.current().route.getName() === 'roomsList') {
      pageSession.set('selectview', 'scopeRoomsTemplate');
    } else if (Router.current().route.getName() === 'gamesList') {
      pageSession.set('selectview', 'scopeGamesTemplate');
    } else {
      pageSession.set('selectview', 'scopeDetailTemplate');
    }
    pageSession.set('scopeId', Router.current().params._id);
    pageSession.set('scope', Router.current().params.scope);
  });

  this.autorun(function () {
    if (Router.current().params.scope && Router.current().params._id) {
      if (Router.current().params.orgaCibleId) {
        Session.setPersistent('orgaCibleId', Router.current().params.orgaCibleId);
      } else if (Router.current().params.scope === 'organizations') {
        Session.setPersistent('orgaCibleId', Router.current().params._id);
        // console.log('news news.js 102');
      }

      const handle = Meteor.subscribe('scopeDetail', Router.current().params.scope, Router.current().params._id);
      if (handle.ready()) {
        if (!Router.current().params.orgaCibleId && Router.current().params.scope !== 'organizations') {
          const countOrga = Organizations.find({}).count();
          const orgaNoName = Organizations.find({
            name: { $exists: false },
          });

          if (countOrga > 1) {
            // console.log('news news.js 117 countOrga', countOrga, Session.get('orgaCibleId'));
            // 1 de plus quand faut changer

            if (Router.current().params.scope === 'projects') {
              const project = `links.projects.${Router.current().params._id}`;
              const countOrgaNow = Organizations.find({ _id: new Mongo.ObjectID(Session.get('orgaCibleId')), [project]: { $exists: 1 } }).count();
              projectsCountOrga({ orgaNoName, countOrgaNow, orgaCibleId: Session.get('orgaCibleId') });
            } else if (Router.current().params.scope === 'events') {
              const event = `links.events.${Router.current().params._id}`;
              const projectOne = Projects.find({ [event]: { $exists: 1 } });

              eventsCountOrga({
                _id: Router.current().params._id, orgaNoName, orgaCibleId: Session.get('orgaCibleId'), projectOne,
              });
            } else if (Router.current().params.scope === 'actions') {
              const actionObjectId = new Mongo.ObjectID(Router.current().params._id);
              const actionOne = Actions.findOne({ _id: actionObjectId });
              const parentObjectId = new Mongo.ObjectID(actionOne.parentId);

              if (actionOne.parentType === 'events') {
                // events
                const projectOne = Projects.findOne({ _id: parentObjectId });
                eventsCountOrga({
                  _id: Router.current().params._id, orgaNoName, orgaCibleId: Session.get('orgaCibleId'), projectOne,
                });
              } else if (actionOne.parentType === 'projects') {
                // projects
                const project = `links.projects.${actionOne.parentId}`;
                const countOrgaNow = Organizations.find({ _id: new Mongo.ObjectID(Session.get('orgaCibleId')), [project]: { $exists: 1 } }).count();
                projectsCountOrga({ orgaNoName, countOrgaNow, orgaCibleId: Session.get('orgaCibleId') });
              } else if (actionOne.parentType === 'organizations') {
                Session.setPersistent('orgaCibleId', actionOne.parentId);
              } else if (actionOne.parentType === 'citoyens') {
                // citoyens
              }
            }
          } else if (countOrga === 1) {
            // console.log('news news.js 144 countOrga === 1', Session.get('orgaCibleId'));
          }
        }
      }
      this.readyScopeDetail.set(handle.ready());
    }
  }.bind(this));
});

Template.newsList.helpers({
  scope() {
    if (Router.current().params.scope) {
      const collection = nameToCollection(Router.current().params.scope);
      return collection.findOne({ _id: new Mongo.ObjectID(Router.current().params._id) });
    }
    // return undefined;
  },
  scopeCardTemplate() {
    return `listCard${Router.current().params.scope}`;
  },
  isVote() {
    return this.type === 'vote';
  },
  dataReadyScopeDetail() {
    return Template.instance().readyScopeDetail.get();
  },
  selectview() {
    return pageSession.get('selectview');
  },
  scopeMe() {
    return pageSession.get('scope') === 'citoyens' && Router.current().params._id === Meteor.userId();
  },
  countMembersToBeValidated() {
    return Counter.get(`listMembersToBeValidated.${Router.current().params._id}.${Router.current().params.scope}`);
  },
});

Template.scopeDetailTemplate.helpers({
  scopeCardTemplate() {
    return `listCard${Router.current().params.scope}`;
  },
  dataReadyScopeDetail() {
    return Template.instance().readyScopeDetail.get();
  },
});

Template.scopeNewsTemplate.onCreated(function () {
  this.ready = new ReactiveVar();

  this.autorun(function () {
    pageSession.set('scopeId', Router.current().params._id);
    pageSession.set('scope', Router.current().params.scope);
  });

  this.autorun(function () {
    if (pageSession.get('limit')) {
      IonLoading.show();
      const handleCounter = newsListSubs.subscribe('newsListCounter', Router.current().params.scope, Router.current().params._id);
      const handle = newsListSubs.subscribe('newsList', Router.current().params.scope, Router.current().params._id, pageSession.get('limit'));
      if (handleCounter.ready() && handle.ready()) {
        IonLoading.hide();
        this.ready.set(handle.ready());
      }
    }
  }.bind(this));
});

Template.scopeNewsTemplate.onRendered(function () {
  const showMoreVisible = () => {
    const $target = $('#showMoreResults');
    if (!$target.length) {
      return;
    }
    const threshold = $('.content.overflow-scroll').scrollTop()
      + $('.content.overflow-scroll').height() + 154;
    /* console.log(`threshold: ${threshold}`);
    console.log(`traget top: ${$target.offset().top}`);
    console.log(`list height: ${$('.content.overflow-scroll .list').height()}`); */
    const heightLimit = $('.content.overflow-scroll .list').height();
    if (heightLimit < threshold) {
      if (!$target.data('visible')) {
        $target.data('visible', true);
        pageSession.set('limit', pageSession.get('limit') + 5);
      }
    } else if ($target.data('visible')) {
      $target.data('visible', false);
    }
  };

  $('.content.overflow-scroll').scroll(showMoreVisible);
});

Template.scopeNewsTemplate.helpers({
  scopeBoutonNewsTemplate() {
    return `boutonNews${Router.current().params.scope}`;
  },
  isLimit(countNews) {
    return countNews > pageSession.get('limit');
  },
  countNews() {
    return Counter.get(`countNews.${Router.current().params._id}`);
  },
  dataReady() {
    return Template.instance().ready.get();
  },
});

Template.scopeFilActusTemplate.onCreated(function () {
  this.ready = new ReactiveVar();

  this.autorun(function () {
    pageSession.set('scopeId', Router.current().params._id);
    pageSession.set('scope', Router.current().params.scope);
  });

  this.autorun(function () {
    if (pageSession.get('limitFilActus')) {
      IonLoading.show();
      const handleCount = filActusSubs.subscribe('citoyenActusListCounter');
      const handle = filActusSubs.subscribe('citoyenActusList', pageSession.get('limitFilActus'));
      if (handle.ready() && handleCount.ready()) {
        IonLoading.hide();
        this.ready.set(handle.ready());
      }
    }
  }.bind(this));
});

Template.scopeFilActusTemplate.onRendered(function () {
  const showMoreVisible = () => {
    const $target = $('#showMoreResultslimitFilActus');
    if (!$target.length) {
      return;
    }
    const threshold = $('.content.overflow-scroll').scrollTop()
      + $('.content.overflow-scroll').height() + 150;
    /* console.log(`threshold: ${threshold}`);
    console.log(`traget top: ${$target.offset().top}`);
    console.log(`list height: ${$('.content.overflow-scroll .list').height()}`); */
    const heightLimit = $('.content.overflow-scroll .list').height();
    if (heightLimit < threshold) {
      if (!$target.data('visiblelimitFilActus')) {
        $target.data('visiblelimitFilActus', true);
        pageSession.set('limitFilActus', pageSession.get('limitFilActus') + 5);
      }
    } else if ($target.data('visiblelimitFilActus')) {
      $target.data('visiblelimitFilActus', false);
    }
  };

  $('.content.overflow-scroll').scroll(showMoreVisible);
});

Template.scopeFilActusTemplate.helpers({
  scopeBoutonNewsTemplate() {
    return `boutonFilActus${Router.current().params.scope}`;
  },
  isLimit(countNews) {
    return countNews > pageSession.get('limitFilActus');
  },
  countNews() {
    return Counter.get(`countActus.${Router.current().params._id}`);
  },
  dataReady() {
    return Template.instance().ready.get();
  },
});

Template.scopeNotificationsTemplate.onCreated(function () {
  this.ready = new ReactiveVar();

  this.autorun(function () {
    pageSession.set('scopeId', Router.current().params._id);
    pageSession.set('scope', Router.current().params.scope);
  });

  this.autorun(function () {
    if (Router.current().params.scope !== 'events') {
      const handleToBeValidated = Meteor.subscribe('listMembersToBeValidated', Router.current().params.scope, Router.current().params._id);
      const handle = Meteor.subscribe('notificationsScope', Router.current().params.scope, Router.current().params._id);
      const handleCount = Meteor.subscribe('notificationsScopeCount', Router.current().params.scope, Router.current().params._id);
      if (handleToBeValidated.ready() && handle.ready() && handleCount.ready()) { this.ready.set(handle.ready()); }
    } else {
      const handle = Meteor.subscribe('notificationsScope', Router.current().params.scope, Router.current().params._id);
      const handleCount = Meteor.subscribe('notificationsScopeCount', Router.current().params.scope, Router.current().params._id);
      if (handle.ready() && handleCount.ready()) { this.ready.set(handle.ready()); }
    }
  }.bind(this));
});

Template.scopeNotificationsTemplate.helpers({
  scopeBoutonNotificationsTemplate() {
    return `boutonNotifications${Router.current().params.scope}`;
  },
  dataReady() {
    return Template.instance().ready.get();
  },
});

Template.scopeNotificationsTemplate.events({
  'click .validateYes'(event, instance) {
    event.preventDefault();
    const scopeId = pageSession.get('scopeId');
    const scope = pageSession.get('scope');
    // console.log(`${scopeId},${scope},${this._id._str},${this.scopeVar()}`);

    const linkOption = instance.data.isAdminPending(this._id._str) ? 'toBeValidated' : 'isAdminPending';
    Meteor.call('validateEntity', scopeId, scope, this._id._str, this.scopeVar(), linkOption, function (err) {
      if (err) {
        if (err.reason) {
          IonPopup.alert({ template: i18n.__(err.reason) });
        }
      } else {
        // console.log('yes validate');
      }
    });
  },
  'click .validateNo'(event) {
    event.preventDefault();
    const scopeId = pageSession.get('scopeId');
    const scope = pageSession.get('scope');
    Meteor.call('disconnectEntity', scopeId, scope, undefined, this._id._str, this.scopeVar(), function (err) {
      if (err) {
        if (err.reason) {
          IonPopup.alert({ template: i18n.__(err.reason) });
        }
      } else {
        // console.log('no validate');
      }
    });
  },
});

Template.scopeProjectsTemplate.onCreated(function () {
  this.ready = new ReactiveVar();

  pageSession.set('sortProjects', null);
  pageSession.set('searchProjects', null);

  this.autorun(function () {
    pageSession.set('scopeId', Router.current().params._id);
    pageSession.set('scope', Router.current().params.scope);
  });

  this.autorun(function () {
    const handle = this.subscribe('directoryListProjects', Router.current().params.scope, Router.current().params._id);
    this.ready.set(handle.ready());
  }.bind(this));
});

Template.scopeProjectsTemplate.events({
  'keyup #search, change #search'(event) {
    if (event.currentTarget.value.length > 2) {
      pageSession.set('searchProjects', event.currentTarget.value);
    } else {
      pageSession.set('searchProjects', null);
    }
  },
});

Template.scopeProjectsTemplate.helpers({
  scopeBoutonProjectsTemplate() {
    return `boutonProjects${Router.current().params.scope}`;
  },
  searchProjects() {
    return pageSession.get('searchProjects');
  },
  dataReady() {
    return Template.instance().ready.get();
  },
});

Template.scopeProjectsContributorsTemplate.onCreated(function () {
  this.ready = new ReactiveVar();

  this.autorun(function () {
    pageSession.set('scopeId', Router.current().params._id);
    pageSession.set('scope', Router.current().params.scope);
  });

  this.autorun(function () {
    const handle = this.subscribe('listContributors', Router.current().params._id);
    this.ready.set(handle.ready());
  }.bind(this));
});

Template.scopeProjectsContributorsTemplate.helpers({
  dataReady() {
    return Template.instance().ready.get();
  },
});

Template.scopeProjectsContributorsTemplate.events({
  'click .project-contributors-action-js'(event) {
    event.preventDefault();
    const self = this;
    IonActionSheet.show({
      titleText: `${i18n.__('contributor')}: ${self.name}`,
      buttons: [{ text: i18n.__('Pass admin') }],
      destructiveText: `${i18n.__('delete')} <i class="icon ion-trash-a"></i>`,
      destructiveButtonClicked() {
        IonPopup.confirm({
          title: i18n.__('delete'),
          template: i18n.__('Delete this contributor'),
          onOk() {
            Meteor.call('deleteContributeur', { scopeId: pageSession.get('scopeId'), contributorId: self._id._str }, (error) => {
              if (error) {
                IonPopup.alert({ template: i18n.__(error.reason) });
              }
            });
          },
          onCancel() {
          },
          cancelText: i18n.__('no'),
          okText: i18n.__('yes'),
        });
        return true;
      },
      cancelText: i18n.__('cancel'),
      cancel() {
      },
      buttonClicked(index) {
        if (index === 0) {
          Meteor.call('changeRoleContributeur', { scopeId: pageSession.get('scopeId'), contributorId: self._id._str, role: 'admin' }, (error) => {
            if (error) {
              IonPopup.alert({ template: i18n.__(error.reason) });
            }
          });
        }
        return true;
      },
    });
  },
  'click .project-contributors-admin-action-js'(event) {
    event.preventDefault();
    const self = this;
    IonActionSheet.show({
      titleText: `${i18n.__('contributor')}: ${self.name}`,
      buttons: [{ text: 'Pass member' }],
      destructiveText: `${i18n.__('delete')} <i class="icon ion-trash-a"></i>`,
      destructiveButtonClicked() {
        IonPopup.confirm({
          title: i18n.__('delete'),
          template: i18n.__('Delete this contributor'),
          onOk() {
            Meteor.call('deleteContributeur', { scopeId: pageSession.get('scopeId'), contributorId: self._id._str }, (error) => {
              if (error) {
                IonPopup.alert({ template: i18n.__(error.reason) });
              }
            });
          },
          onCancel() {
          },
          cancelText: i18n.__('no'),
          okText: i18n.__('yes'),
        });
        return true;
      },
      cancelText: i18n.__('cancel'),
      cancel() {
      },
      buttonClicked(index) {
        if (index === 0) {
          Meteor.call('changeRoleContributeur', { scopeId: pageSession.get('scopeId'), contributorId: self._id._str, role: 'contributor' }, (error) => {
            if (error) {
              IonPopup.alert({ template: i18n.__(error.reason) });
            }
          });
        }
        return true;
      },
    });
  },
});

Template.scopeProjectMilestonesTemplate.onCreated(function () {
  this.ready = new ReactiveVar();

  pageSession.set('selectsubview', 'open');
  searchAction.set('searchMilestone', '');

  this.autorun(function () {
    pageSession.set('scopeId', Router.current().params._id);
    pageSession.set('scope', Router.current().params.scope);
  });

  /* this.autorun(function () {
    const handle = this.subscribe('listContributors', Router.current().params._id);
    this.ready.set(handle.ready());
  }.bind(this)); */
});

Template.scopeProjectMilestonesTemplate.helpers({
  dataReady() {
    return Template.instance().ready.get();
  },
  selectsubview() {
    return pageSession.get('selectsubview');
  },
  search() {
    if (searchAction.get('searchMilestone') && searchAction.get('searchMilestone').charAt(0) === '~' && searchAction.get('searchMilestone').length > 1) {
      return searchAction.get('searchMilestone').substr(1);
    }
    return searchAction.get('searchMilestone');
  },
  searchSort() {
    return searchAction.get('searchMilestoneSort');
  },
  boutonsAction() {
    return { event: 'button-positive action-project-milestone-js', icon: 'fa fa-cog' };
  },
});

Template.scopeProjectMilestonesTemplate.events({
  'click .change-selectsubview-js'(event) {
    event.preventDefault();
    pageSession.set('selectsubview', event.currentTarget.id);
  },
});

Template.milestoneAssignActionsModal.onCreated(function () {
  const template = Template.instance();
  const dataContext = Template.currentData();
  template.ready = new ReactiveVar();
  this.autorun(function () {
    if (dataContext) {
      template.scope = dataContext.scope;
      template._id = dataContext._id;
      template.milestoneId = dataContext.milestoneId;
    } else {
      template.scope = Router.current().params.scope;
      template._id = Router.current().params._id;
      template.milestoneId = Router.current().params.milestoneId;
    }

    pageSession.set('scopeId', template._id);
    pageSession.set('scope', template.scope);
    pageSession.set('milestoneId', template.milestoneId);
    if (template.scope && template._id) {
      const handle = Meteor.subscribe('directoryListActions', template.scope, template._id, 'todo', 'noMilestone');
      if (handle.ready()) {
        if (Organizations.find({}).count() === 2) {
          const orgaOne = Organizations.findOne({
            name: { $exists: false },
          });
          if (orgaOne && Session.get('orgaCibleId') !== orgaOne._id._str) {
            Session.setPersistent('orgaCibleId', orgaOne._id._str);
          }
        }
        template.ready.set(handle.ready());
      }
    }
  });
});

Template.milestoneAssignActionsModal.helpers({
  scope() {
    if (Template.instance().scope && Template.instance()._id && Template.instance().ready.get()) {
      const collection = nameToCollection(Template.instance().scope);
      return collection.findOne({ _id: new Mongo.ObjectID(Template.instance()._id) });
    }
    return undefined;
  },
  search() {
    return searchAction.get('search');
  },
  searchSort() {
    return searchAction.get('searchSort');
  },
  scopeVar() {
    return pageSession.get('scope');
  },
  scopeId() {
    return pageSession.get('scopeId');
  },
  milestoneId() {
    return pageSession.get('milestoneId');
  },
  boutonsAction() {
    return { event: 'button-positive assign-jalon-action-js', icon: 'fa fa-check' };
  },
  dataReady() {
    return Template.instance().ready.get();
  },
});

Template.milestoneMoveActionsModal.onCreated(function () {
  const template = Template.instance();
  const dataContext = Template.currentData();
  template.ready = new ReactiveVar();
  this.autorun(function () {
    if (dataContext) {
      template.scope = dataContext.scope;
      template._id = dataContext._id;
      template.milestoneId = dataContext.milestoneId;
      template.milestoneName = dataContext.milestoneName;
    } else {
      template.scope = Router.current().params.scope;
      template._id = Router.current().params._id;
      template.milestoneId = Router.current().params.milestoneId;
    }

    pageSession.set('scopeId', template._id);
    pageSession.set('scope', template.scope);
    pageSession.set('milestoneId', template.milestoneId);
    pageSession.set('milestoneName', template.milestoneName);
  });
});

Template.milestoneMoveActionsModal.helpers({
  scope() {
    if (Template.instance().scope && Template.instance()._id) {
      const collection = nameToCollection(Template.instance().scope);
      return collection.findOne({ _id: new Mongo.ObjectID(Template.instance()._id) });
    }
    return undefined;
  },
  selectsubview() {
    return pageSession.get('selectsubview');
  },
  milestoneName() {
    return pageSession.get('milestoneName');
  },
  milestoneId() {
    return pageSession.get('milestoneId');
  },
  search() {
    if (searchAction.get('searchMilestone') && searchAction.get('searchMilestone').charAt(0) === '~' && searchAction.get('searchMilestone').length > 1) {
      return searchAction.get('searchMilestone').substr(1);
    }
    return searchAction.get('searchMilestone');
  },
  searchSort() {
    return searchAction.get('searchMilestoneSort');
  },
  boutonsAction() {
    return { event: 'button-positive move-jalon-action-js', icon: 'fa fa-check' };
  },
});

Template.milestoneItem.helpers({
  formatMilestoneDate(date) {
    return date && moment(date).format('L');
  },
  isStartDate() {
    if (this.milestone.startDate) {
      const start = moment(this.milestone.startDate).toDate();
      return moment(start).isBefore(); // True
    }
    return false;
  },
  isNotStartDate() {
    if (this.milestone.startDate) {
      const start = moment(this.milestone.startDate).toDate();
      return moment().isBefore(start); // True
    }
    return false;
  },
  isEndDate() {
    if (this.milestone.endDate) {
      const end = moment(this.milestone.endDate).toDate();
      return moment(end).isBefore(); // True
    }
    return false;
  },
  isNotEndDate() {
    if (this.milestone.endDate) {
      const end = moment(this.milestone.endDate).toDate();
      return moment().isBefore(end); // True
    }
    return false;
  },
  isInProgress() {
    if (this.milestone.startDate && this.milestone.endDate) {
      const start = moment(this.milestone.startDate).toDate();
      const end = moment(this.milestone.endDate).toDate();
      return moment().isBefore(end) && moment(start).isBefore(); // True
    } if (this.milestone.startDate && !this.milestone.endDate) {
      const start = moment(this.milestone.startDate).toDate();
      return moment(start).isBefore(); // True
    }
    return false;
  },
  badgeStatus(status) {
    return status && status === 'open' ? 'balanced' : 'assertive';
  },
});

Template.milestoneItem.events({
  'click .action-project-milestone-js'(event) {
    event.preventDefault();
    const self = this;
    if (self.milestone.status === 'open') {
      IonActionSheet.show({
        titleText: i18n.__('Milestones'),
        buttons: [
          { text: `${i18n.__('Create an action on this milestone')}` },
          { text: `${i18n.__('Assign to all actions without milestone')}` },
          { text: `${i18n.__('Assign to actions without milestone')}` },
          { text: `${i18n.__('Move unfinished actions to another milestone')}` },
        ],
        destructiveText: `${i18n.__('Close milestone')} <i class="icon ion-close"></i>`,
        destructiveButtonClicked() {
          // console.log('Edit!');
          IonPopup.confirm({
            title: i18n.__('Close'),
            template: i18n.__('Close the milestone of this project'),
            onOk() {
              Meteor.call('milestoneStatus', { scopeId: self.scope._id._str, milestoneId: self.milestone.milestoneId, status: 'close' }, (error) => {
                if (error) {
                  IonToast.show({
                    title: i18n.__('Error'), position: 'top', type: 'error', showClose: true, template: `<i class="icon fa fa-exclamation-circle"></i> ${error && error.reason ? error.reason.replace(': ', '') : error.error}`,
                  });
                } else {
                  IonToast.show({
                    template: i18n.__('Close milestone'), position: 'bottom', type: 'success', showClose: false, title: `<i class="icon ion-checkmark"></i> ${i18n.__('this milestone has been closed')}`,
                  });
                }
              });
            },
            onCancel() {
            },
            cancelText: i18n.__('no'),
            okText: i18n.__('yes'),
          });
          return true;
        },
        cancelText: i18n.__('cancel'),
        cancel() {
          // console.log('Cancelled!');
        },
        buttonClicked(index) {
          if (index === 0) {
            pageSessionAction.set('milestoneId', self.milestone.milestoneId);
            Router.go('actionsAdd', { _id: self.scope._id._str, scope: 'projects' });
          }
          if (index === 1) {
            Meteor.call('milestoneAssignAllActions', { scopeId: self.scope._id._str, milestoneId: self.milestone.milestoneId }, (error) => {
              if (error) {
                IonToast.show({
                  title: i18n.__('Error'), position: 'top', type: 'error', showClose: true, template: `<i class="icon fa fa-exclamation-circle"></i> ${error && error.reason ? error.reason.replace(': ', '') : error.error}`,
                });
              } else {
                IonToast.show({
                  template: i18n.__('Milestone assignment'), position: 'bottom', type: 'success', showClose: false, title: `<i class="icon ion-checkmark"></i> ${i18n.__('all actions without a milestone have been assigned to this milestone')}`,
                });
              }
            });
          }
          if (index === 2) {
            const parentDataContext = { _id: self.scope._id._str, scope: 'projects', milestoneId: self.milestone.milestoneId };
            IonModal.open('milestoneAssignActionsModal', parentDataContext);
          }
          if (index === 3) {
            const parentDataContext = {
              _id: self.scope._id._str, scope: 'projects', milestoneId: self.milestone.milestoneId, milestoneName: self.milestone.name,
            };
            IonModal.open('milestoneMoveActionsModal', parentDataContext);
          }
          return true;
        },
      });
    } else if (self.milestone.status === 'close') {
      IonActionSheet.show({
        titleText: i18n.__('Milestones'),
        buttons: [
          { text: `${i18n.__('Reopen the milestone')}` },
        ],
        cancelText: i18n.__('cancel'),
        cancel() {
          // console.log('Cancelled!');
        },
        buttonClicked(index) {
          if (index === 0) {
            Meteor.call('milestoneStatus', { scopeId: self.scope._id._str, milestoneId: self.milestone.milestoneId, status: 'open' }, (error) => {
              if (error) {
                IonToast.show({
                  title: i18n.__('Error'), position: 'top', type: 'error', showClose: true, template: `<i class="icon fa fa-exclamation-circle"></i> ${error && error.reason ? error.reason.replace(': ', '') : error.error}`,
                });
              } else {
                IonToast.show({
                  template: i18n.__('Reopen the milestone'), position: 'bottom', type: 'success', showClose: false, title: `<i class="icon ion-checkmark"></i> ${i18n.__('this milestone has been reopened')}`,
                });
              }
            });
          }
          return true;
        },
      });
    }
  },
  'click .move-jalon-action-js'(event) {
    event.preventDefault();
    const self = this;
    Meteor.call('milestoneMoveActions', { scopeId: self.scope._id._str, oldMilestoneId: self.oldMilestoneId, milestoneId: self.milestone.milestoneId }, (error) => {
      if (error) {
        IonToast.show({
          title: i18n.__('Error'), position: 'top', type: 'error', showClose: true, template: `<i class="icon fa fa-exclamation-circle"></i> ${error && error.reason ? error.reason.replace(': ', '') : error.error}`,
        });
      } else {
        IonToast.show({
          template: i18n.__('Move unfinished actions to another milestone'), position: 'bottom', type: 'success', showClose: false, title: `<i class="icon ion-checkmark"></i> ${i18n.__('the unfinished movement of actions has been made')}`,
        });
      }
    });
  },
});

Template.scopeActionsTemplate.onCreated(function () {
  this.ready = new ReactiveVar();
  this.autorun(function () {
    pageSession.set('scopeId', Router.current().params._id);
    pageSession.set('scope', Router.current().params.scope);
    if (Router.current().params.scope === 'citoyens') {
      const handle = this.subscribe('all.user.actions2', Router.current().params.scope, Router.current().params._id);
      this.ready.set(handle.ready());
    } else {
      const handle = this.subscribe('directoryListActions', Router.current().params.scope, Router.current().params._id, 'todo');
      this.ready.set(handle.ready());
    }
  }.bind(this));
});

Template.scopeActionsTemplate.helpers({
  scopeBoutonActionsTemplate() {
    return `boutonActions${Router.current().params.scope}`;
  },
  selectsubview() {
    return pageSession.get('selectsubview');
  },
  search() {
    return searchAction.get('search');
  },
  searchSort() {
    return searchAction.get('searchSort');
  },
  searchMaintenance() {
    return searchAction.get('searchMaintenance');
  },
  scopeVar() {
    return pageSession.get('scope');
  },
  scopeId() {
    return pageSession.get('scopeId');
  },
  dataReady() {
    return Template.instance().ready.get();
  },
});

Template.scopeActionsTemplate.events({
  'click .change-selectsubview-js'(event) {
    event.preventDefault();
    pageSession.set('selectsubview', event.currentTarget.id);
  },
});

Template.scopeAssessTemplate.onCreated(function () {
  this.ready = new ReactiveVar();
  this.autorun(function () {
    pageSession.set('scopeId', Router.current().params._id);
    pageSession.set('scope', Router.current().params.scope);
    const handle = this.subscribe('directoryListAnswers', Router.current().params.scope, Router.current().params._id);
    this.ready.set(handle.ready());
  }.bind(this));
});

Template.scopeAssessTemplate.helpers({
  scopeBoutonAssessTemplate() {
    return `boutonAssess${Router.current().params.scope}`;
  },
  scopeVar() {
    return pageSession.get('scope');
  },
  scopeId() {
    return pageSession.get('scopeId');
  },
  dataReady() {
    return Template.instance().ready.get();
  },
});

Template.scopeAnswersTemplate.onCreated(function () {
  this.ready = new ReactiveVar();
  this.autorun(function () {
    pageSession.set('scopeId', Router.current().params._id);
    pageSession.set('scope', Router.current().params.scope);
    const handle = this.subscribe('directoryListAnswers', Router.current().params.scope, Router.current().params._id);
    this.ready.set(handle.ready());
  }.bind(this));
});

Template.scopeAnswersTemplate.helpers({
  scopeBoutonAnswersTemplate() {
    return `boutonAnswers${Router.current().params.scope}`;
  },
  search() {
    return pageSession.get('search');
  },
  searchSort() {
    return pageSession.get('searchSort');
  },
  scopeVar() {
    return pageSession.get('scope');
  },
  scopeId() {
    return pageSession.get('scopeId');
  },
  dataReady() {
    return Template.instance().ready.get();
  },
});

Template.searchAnswers.onRendered(function () {
  const wrap = $('.view .content.overflow-scroll');
  const search = $('.view .content.overflow-scroll #search');
  if (wrap.length > 0 && search.length > 0) {
    wrap.on('scroll', function () {
      if (!Platform.isIOS()) {
        if (this.scrollTop > 147) {
          wrap.addClass('fix-search');
        }
        if (this.scrollTop < 82) {
          wrap.removeClass('fix-search');
        }
      }
    });
  } else {
    const wrapModal = $('.modal .content.overflow-scroll');
    const searchModal = $('.modal .content.overflow-scroll #search');
    if (wrapModal.length > 0 && searchModal.length > 0) {
      wrapModal.on('scroll', function () {
        if (!Platform.isIOS()) {
          if (this.scrollTop > 147) {
            wrapModal.addClass('fix-search');
          }
          if (this.scrollTop < 82) {
            wrapModal.removeClass('fix-search');
          }
        }
      });
    }
  }
});

Template.searchAnswers.helpers({
  search() {
    return pageSession.get('search');
  },
  searchHelp() {
    return pageSession.get('searchHelp');
  },
  searchTag() {
    return pageSession.get('searchTag');
  },
  searchStatus() {
    return pageSession.get('searchStatus');
  },
  searchAcceptation() {
    return pageSession.get('searchAcceptation');
  },
  searchPriority() {
    return pageSession.get('searchPriority');
  },
  sortActived() {
    if (pageSession.get('searchSort')) {
      return searchQuerySortActived(pageSession.get('searchSort'));
    }
    return false;
  },
});

Template.scopeItemListAnswer.onRendered(function () {
  // eslint-disable-next-line no-new
  lazy.lazyLoad();
});

Template.scopeItemListAnswer.events({
  'click .searchtag-js'(event) {
    event.preventDefault();
    if (this) {
      pageSession.set('searchHelp', null);
      pageSession.set('searchStatus', null);
      pageSession.set('searchAcceptation', null);
      pageSession.set('searchPriority', null);
      pageSession.set('search', `#${this}`);
    } else {
      pageSession.set('search', null);
    }
  },
  'click .searchstatus-js'(event) {
    event.preventDefault();
    if (this) {
      pageSession.set('searchHelp', null);
      pageSession.set('searchTag', null);
      pageSession.set('searchAcceptation', null);
      pageSession.set('searchPriority', null);
      pageSession.set('search', `!${this.status}`);
    } else {
      pageSession.set('search', null);
    }
  },
  'click .searchacceptation-js'(event) {
    event.preventDefault();
    if (this) {
      pageSession.set('searchHelp', null);
      pageSession.set('searchTag', null);
      pageSession.set('searchStatus', null);
      pageSession.set('searchPriority', null);
      pageSession.set('search', `>${this.status}`);
    } else {
      pageSession.set('search', null);
    }
  },
  'click .searchpriority-js'(event) {
    event.preventDefault();
    if (this) {
      pageSession.set('searchHelp', null);
      pageSession.set('searchTag', null);
      pageSession.set('searchStatus', null);
      pageSession.set('searchAcceptation', null);
      pageSession.set('search', `~${this.status}`);
    } else {
      pageSession.set('search', null);
    }
  },
});

Template.searchAnswers.events({
  'click .searchstatus-js'(event) {
    event.preventDefault();
    pageSession.set('searchHelp', null);
    pageSession.set('searchTag', null);
    pageSession.set('searchStatus', null);
    pageSession.set('searchAcceptation', null);
    pageSession.set('searchPriority', null);
    if (this) {
      pageSession.set('search', `!${this.status}`);
    } else {
      pageSession.set('search', null);
    }
  },
  'click .searchacceptation-js'(event) {
    event.preventDefault();
    pageSession.set('searchHelp', null);
    pageSession.set('searchTag', null);
    pageSession.set('searchStatus', null);
    pageSession.set('searchAcceptation', null);
    pageSession.set('searchPriority', null);
    if (this) {
      pageSession.set('search', `>${this.status}`);
    } else {
      pageSession.set('search', null);
    }
  },
  'click .searchpriority-js'(event) {
    pageSession.set('searchHelp', null);
    pageSession.set('searchTag', null);
    pageSession.set('searchStatus', null);
    pageSession.set('searchAcceptation', null);
    pageSession.set('searchPriority', null);
    event.preventDefault();
    if (this) {
      pageSession.set('search', `~${this.status}`);
    } else {
      pageSession.set('search', null);
    }
  },
  'click .searchfilter-js'(event) {
    event.preventDefault();
    const filter = $(event.currentTarget).data('action');
    if (filter) {
      pageSession.set('searchTag', null);
      pageSession.set('searchStatus', null);
      pageSession.set('searchAcceptation', null);
      pageSession.set('searchPriority', null);
      pageSession.set('search', filter);
      pageSession.set('searchHelp', null);
    } else {
      pageSession.set('search', null);
    }
  },
  'keyup #search, change #search': _.debounce((event) => {
    if (event.currentTarget.value.length > 0) {
      // console.log(event.currentTarget.value);

      if (event.currentTarget.value.charAt(0) === '#') {
        pageSession.set('searchTag', event.currentTarget.value);
      } else {
        pageSession.set('searchTag', null);
      }

      if (event.currentTarget.value.charAt(0) === '!') {
        pageSession.set('searchStatus', event.currentTarget.value);
      } else {
        pageSession.set('searchStatus', null);
      }

      if (event.currentTarget.value.charAt(0) === '>') {
        pageSession.set('searchAcceptation', event.currentTarget.value);
      } else {
        pageSession.set('searchAcceptation', null);
      }

      if (event.currentTarget.value.charAt(0) === '~') {
        pageSession.set('searchPriority', event.currentTarget.value);
      } else {
        pageSession.set('searchPriority', null);
      }

      if (event.currentTarget.value.length === 1 && event.currentTarget.value.charAt(0) === '?') {
        pageSession.set('searchHelp', true);
      } else {
        pageSession.set('searchHelp', null);
      }

      pageSession.set('search', event.currentTarget.value);
    } else {
      pageSession.set('searchHelp', null);
      pageSession.set('search', null);
      pageSession.set('searchTag', null);
      pageSession.set('searchStatus', null);
      pageSession.set('searchAcceptation', null);
      pageSession.set('searchPriority', null);
    }
  }, 500),
});

Template.searchAnswersSort.helpers({
  searchSort() {
    if (pageSession.get('searchSort')) {
      return pageSession.get('searchSort');
    }
    // Todo : translate label
    const sortDefault = {
      answers: [
        {
          label: i18n.__('sortModal.activity_date'), type: 'answers', field: 'updated', existField: true, checked: false, fieldDesc: false,
        },
        {
          label: i18n.__('sortModal.creation_date'), type: 'answers', field: 'created', existField: true, checked: false, fieldDesc: false,
        },
      ],
    };
    pageSession.set('searchSort', sortDefault);
    return pageSession.get('searchSort');
  },
  orderType(type) {
    if (type) {
      const sort = pageSession.get('searchSort');
      return sort && sort[type] ? [...sort[type]].filter((item) => item.checked === true).sort(compareValues('order')) : [];
    }
  },
});

Template.searchAnswersSortItemToggle.events({
  'click .sort-checked-js'(event) {
    const self = this;
    if (this.type) {
      const sort = pageSession.get('searchSort');
      const arrayOrder = sort[this.type].filter((item) => item.checked === true);
      const countOrder = arrayOrder && arrayOrder.length > 0 ? arrayOrder.length : 0;
      sort[this.type] = sort[this.type].map((item) => {
        if (item.field === self.field) {
          item.checked = event.currentTarget.checked;
          if (event.currentTarget.checked) {
            item.order = countOrder + 1;
          } else {
            delete item.order;
          }
          return item;
        }
        return item;
      });
      pageSession.set('searchSort', sort);
    }
  },
  'click .sort-desc-js'(event) {
    const self = this;
    if (this.type) {
      const sort = pageSession.get('searchSort');
      sort[this.type] = sort[this.type].map((item) => {
        if (item.field === self.field) {
          item.fieldDesc = event.currentTarget.checked;
          return item;
        }
        return item;
      });
      pageSession.set('searchSort', sort);
    }
  },
});

Template.scopeOrganizationsTemplate.onCreated(function () {
  this.ready = new ReactiveVar();

  this.autorun(function () {
    pageSession.set('scopeId', Router.current().params._id);
    pageSession.set('scope', Router.current().params.scope);
  });

  this.autorun(function () {
    const handle = this.subscribe('directoryListOrganizations', Router.current().params.scope, Router.current().params._id);
    this.ready.set(handle.ready());
  }.bind(this));
});

Template.scopeOrganizationsTemplate.helpers({
  scopeBoutonProjectsTemplate() {
    return `boutonOrganizations${Router.current().params.scope}`;
  },
  dataReady() {
    return Template.instance().ready.get();
  },
});

Template.scopeOrganizationsTemplate.events({
  'click .copie-config-oceco-js'(event) {
    event.preventDefault();
    Meteor.call('copieConfigOceco', { scopeId: this._id._str, scope: 'organizations' }, function (error, result) {
      if (result) {
        IonToast.show({
          template: i18n.__('The oceco config of the parent organization has been copied to the sub-organization'), position: 'bottom', type: 'success', showClose: false, title: `<i class="icon ion-checkmark"></i> ${i18n.__('config oceco')}`,
        });
      }
    });
  },
  'click .action-config-oceco-js'(event) {
    event.preventDefault();
    // info,description,contact
    const self = this;
    if (self.oceco) {
      IonActionSheet.show({
        titleText: i18n.__('Config oceco sub-organization'),
        buttons: [
          { text: `${i18n.__('invite members or admin of the parent organization')}` },
        ],
        destructiveText: `${i18n.__('Delete the oceco config')} <i class="icon ion-trash-a"></i>`,
        destructiveButtonClicked() {
          // console.log('Edit!');
          IonPopup.confirm({
            title: i18n.__('delete'),
            template: i18n.__('Delete the oceco config from this sub-organization'),
            onOk() {
              Meteor.call('deleteConfigOceco', { scopeId: self._id._str, scope: 'organizations' }, (error) => {
                if (error) {
                  IonToast.show({
                    title: i18n.__('Error'), position: 'top', type: 'error', showClose: true, template: `<i class="icon fa fa-exclamation-circle"></i> ${error && error.reason ? error.reason.replace(': ', '') : error.error}`,
                  });
                } else {
                  IonToast.show({
                    template: i18n.__('deleted'), position: 'bottom', type: 'success', showClose: false, title: `<i class="icon ion-checkmark"></i> ${i18n.__('Config oceco sub- organization')}`,
                  });
                }
              });
            },
            onCancel() {
            },
            cancelText: i18n.__('no'),
            okText: i18n.__('yes'),
          });
          return true;
        },
        cancelText: i18n.__('cancel'),
        cancel() {
          // console.log('Cancelled!');
        },
        buttonClicked(index) {
          if (index === 0) {
            Router.go('invitations', { _id: self._id._str, scope: 'organizations' });
          }
          if (index === 1) {
            // console.log('Edit!');

          }
          if (index === 2) {
            // console.log('Edit!');

          }

          return true;
        },
      });
    } else {
      IonActionSheet.show({
        titleText: i18n.__('Config oceco sub-organization'),
        buttons: [
          { text: `${i18n.__('copy the mother oceco config')}` },
        ],
        cancelText: i18n.__('cancel'),
        cancel() {
          // console.log('Cancelled!');
        },
        buttonClicked(index) {
          if (index === 0) {
            // console.log('Edit!');
            Meteor.call('copieConfigOceco', { scopeId: self._id._str, scope: 'organizations' }, function (error, result) {
              if (result) {
                IonToast.show({
                  template: i18n.__('The oceco config of the parent organization has been copied to the sub-organizationa'), position: 'bottom', type: 'success', showClose: false, title: `<i class="icon ion-checkmark"></i> ${i18n.__('Config oceco sub-organization')}`,
                });
              }
            });
          }
          return true;
        },
      });
    }
  },
});

Template.scopeEventsTemplate.onCreated(function () {
  this.ready = new ReactiveVar();

  this.autorun(function () {
    pageSession.set('scopeId', Router.current().params._id);
    pageSession.set('scope', Router.current().params.scope);
  });

  this.autorun(function () {
    const handle = newsListSubs.subscribe('directoryListEvents', Router.current().params.scope, Router.current().params._id);
    this.ready.set(handle.ready());
  }.bind(this));
});

Template.scopeEventsTemplate.helpers({
  scopeBoutonEventsTemplate() {
    return `boutonEvents${Router.current().params.scope}`;
  },
  dataReady() {
    return Template.instance().ready.get();
  },
});

Template.scopeFormsTemplate.onCreated(function () {
  this.ready = new ReactiveVar();

  this.autorun(function () {
    pageSession.set('scopeId', Router.current().params._id);
    pageSession.set('scope', Router.current().params.scope);
  });

  this.autorun(function () {
    const handle = Meteor.subscribe('directoryListForms', Router.current().params.scope, Router.current().params._id);
    // info : test si form = 1
    const orgaOne = Organizations.findOne({ _id: new Mongo.ObjectID(Router.current().params._id) });
    if (handle.ready()) {
      if (orgaOne.countForms() === 1) {
        const listForms = orgaOne.listForms().fetch();
        Router.go('answersListOrga', { orgaCibleId: Router.current().params._id, _id: listForms[0]._id._str, scope: 'forms' }, { replaceState: true });
      } else {
        this.ready.set(handle.ready());
      }
    }
  }.bind(this));
});

Template.scopeFormsTemplate.helpers({
  scopeBoutonFormsTemplate() {
    return `boutonForms${Router.current().params.scope}`;
  },
  dataReady() {
    return Template.instance().ready.get();
  },
});

Template.scopeProjectsEventsTemplate.onCreated(function () {
  this.ready = new ReactiveVar();

  this.autorun(function () {
    pageSession.set('scopeId', Router.current().params._id);
    pageSession.set('scope', Router.current().params.scope);
  });

  this.autorun(function () {
    const handle = newsListSubs.subscribe('directoryProjectsListEvents', Router.current().params.scope, Router.current().params._id);
    this.ready.set(handle.ready());
  }.bind(this));
});

Template.scopeProjectsEventsTemplate.helpers({
  scopeBoutonEventsTemplate() {
    return `boutonEvents${Router.current().params.scope}`;
  },
  dataReady() {
    return Template.instance().ready.get();
  },
});

Template.scopeGamesTemplate.onCreated(function () {
  this.ready = new ReactiveVar();

  this.autorun(function () {
    pageSession.set('scopeId', Router.current().params._id);
    pageSession.set('scope', Router.current().params.scope);
  });

  this.autorun(function () {
    const handle = newsListSubs.subscribe('directoryListGames', Router.current().params.scope, Router.current().params._id);
    this.ready.set(handle.ready());
  }.bind(this));
});

Template.scopeGamesTemplate.helpers({
  scopeBoutonGamesTemplate() {
    return `boutonGames${Router.current().params.scope}`;
  },
  dataReady() {
    return Template.instance().ready.get();
  },
});

Template.scopeRoomsTemplate.onCreated(function () {
  this.ready = new ReactiveVar();

  this.autorun(function () {
    pageSession.set('scopeId', Router.current().params._id);
    pageSession.set('scope', Router.current().params.scope);
  });

  this.autorun(function () {
    const handle = newsListSubs.subscribe('directoryListRooms', Router.current().params.scope, Router.current().params._id);
    this.ready.set(handle.ready());
  }.bind(this));
});

Template.scopeRoomsTemplate.helpers({
  scopeBoutonEventsTemplate() {
    return `boutonRooms${Router.current().params.scope}`;
  },
  dataReady() {
    return Template.instance().ready.get();
  },
});

Template.addMenuScopePopover.helpers({
  scopeVar() {
    return pageSession.get('scope');
  },
  scopeId() {
    return pageSession.get('scopeId');
  },
});

Template.newsList.events({
  'click .selectview'(event) {
    event.preventDefault();
    pageSession.set('selectview', event.currentTarget.id);
  },
  /*
  'click .saveattendees-link' (event) {
    event.preventDefault();
    const scopeId = pageSession.get('scopeId');
    Meteor.call('saveattendeesEvent', scopeId);
  },
  'click .inviteattendees-link' (event) {
    event.preventDefault();
    const scopeId = pageSession.get('scopeId');
    Meteor.call('inviteattendeesEvent', scopeId);
  },
  'click .connectscope-link' (event) {
    event.preventDefault();
    const scopeId = pageSession.get('scopeId');
    const scope = pageSession.get('scope');
    Meteor.call('connectEntity', scopeId, scope);
  }
  ,
  'click .disconnectscope-link' (event) {
    event.preventDefault();
    const scopeId = pageSession.get('scopeId');
    const scope = pageSession.get('scope');
    Meteor.call('disconnectEntity', scopeId, scope);
  }
  ,
  'click .followperson-link' (event) {
    event.preventDefault();
    const scopeId = pageSession.get('scopeId');
    const scope = pageSession.get('scope');
    Meteor.call('followEntity', scopeId, scope);
  },
  'click .unfollowperson-link' (event) {
    event.preventDefault();
    const scopeId = pageSession.get('scopeId');
    const scope = pageSession.get('scope');
    Meteor.call('disconnectEntity', scopeId, scope);
  },
  'click .unfollowscope-link' (event) {
    event.preventDefault();
    const scopeId = pageSession.get('scopeId');
    const scope = pageSession.get('scope');
    Meteor.call('disconnectEntity', scopeId, scope, 'followers');
  }, */
  'click .give-me-more'() {
    const newLimit = pageSession.get('limit') + 5;
    pageSession.set('limit', newLimit);
  },
  'click .give-me-more-actus'() {
    const newLimit = pageSession.get('limitFilActus') + 5;
    pageSession.set('limitFilActus', newLimit);
  },
  'click .photo-link-new'(event, instance) {
    const self = this;
    const scope = pageSession.get('scope');

    if (Meteor.isDesktop) {
      pageSession.set('newsId', null);
      instance.$('#file-upload-new').trigger('click');
    } else if (Meteor.isCordova) {
      const options = {
        width: 640,
        height: 480,
        quality: 75,
      };

      const successCallback = (retour) => {
        const newsId = retour;
        IonPopup.confirm({
          title: i18n.__('Photo'),
          template: i18n.__('Do you want to add another photo to this news'),
          onOk() {
            MeteorCameraUI.getPicture(options, function (error, data) {
              if (!error) {
                const str = `${+new Date() + Math.floor((Math.random() * 100) + 1)}.jpg`;
                Meteor.call('photoNews', data, str, scope, self._id._str, newsId, function (errorCall, result) {
                  if (!errorCall) {
                    successCallback(result.newsId);
                  } else {
                    // console.log('error',error);
                  }
                });
              }
            });
          },
          onCancel() {
            Router.go('newsList', { _id: self._id._str, scope });
          },
          cancelText: i18n.__('finish'),
          okText: i18n.__('other picture'),
        });
      };

      MeteorCameraUI.getPicture(options, function (error, data) {
        if (!error) {
          const str = `${+new Date() + Math.floor((Math.random() * 100) + 1)}.jpg`;
          Meteor.call('photoNews', data, str, scope, self._id._str, function (errorCall, result) {
            if (!errorCall) {
              successCallback(result.newsId);
            } else {
              // console.log('error',error);
            }
          });
        }
      });
    } else {
      pageSession.set('newsId', null);
      instance.$('#file-upload-new').trigger('click');
    }
  },
  'click .photo-link-scope'(event, instance) {
    event.preventDefault();
    const self = this;
    const scope = pageSession.get('scope');
    if (Meteor.isDesktop) {
      instance.$('#file-upload').trigger('click');
    } else if (Meteor.isCordova) {
      const options = {
        width: 640,
        height: 480,
        quality: 75,
      };
      MeteorCameraUI.getPicture(options, function (error, data) {
        if (!error) {
          const str = `${+new Date() + Math.floor((Math.random() * 100) + 1)}.jpg`;
          Meteor.call('photoScope', scope, data, str, self._id._str, function (errorPhoto) {
            if (!errorPhoto) {
              // console.log(result);
            } else {
              // console.log('error', error);
            }
          });
        }
      });
    } else {
      instance.$('#file-upload').trigger('click');
    }
  },
  'change #file-upload'(event, instance) {
    event.preventDefault();
    const self = this;
    const scope = pageSession.get('scope');
    if (window.File && window.FileReader && window.FileList && window.Blob) {
      _.each(instance.find('#file-upload').files, function (file) {
        if (file.size > 1) {
          const reader = new FileReader();
          reader.onload = function () {
            // console.log(file.name);
            // console.log(file.type);
            // console.log(reader.result);
            // let str = +new Date + Math.floor((Math.random() * 100) + 1) + ".jpg";
            const str = file.name;
            const dataURI = reader.result;
            Meteor.call('photoScope', scope, dataURI, str, self._id._str, function (error) {
              if (!error) {
                // console.log(result);
              } else {
                // console.log('error',error);
              }
            });
          };
          reader.readAsDataURL(file);
        }
      });
    }
  },
  'change #file-upload-new'(event, instance) {
    event.preventDefault();
    const self = this;
    const scope = pageSession.get('scope');
    const newsId = pageSession.get('newsId');

    function successCallback(retour) {
      const newsId = retour;
      IonPopup.confirm({
        title: i18n.__('Photo'),
        template: i18n.__('Do you want to add another photo to this news'),
        onOk() {
          pageSession.set('newsId', newsId);
          instance.$('#file-upload-new').trigger('click');
        },
        onCancel() {
          Router.go('newsList', { _id: self._id._str, scope });
        },
        cancelText: i18n.__('finish'),
        okText: i18n.__('other picture'),
      });
    }

    if (window.File && window.FileReader && window.FileList && window.Blob) {
      _.each(instance.find('#file-upload-new').files, function (file) {
        if (file.size > 1) {
          const reader = new FileReader();
          reader.onload = function () {
            // console.log(file.name);
            // console.log(file.type);
            // console.log(reader.result);
            // let str = +new Date + Math.floor((Math.random() * 100) + 1) + ".jpg";
            const str = file.name;
            const dataURI = reader.result;
            if (newsId) {
              Meteor.call('photoNews', dataURI, str, scope, self._id._str, newsId, function (error, result) {
                if (!error) {
                  successCallback(result.newsId);
                } else {
                  // console.log('error',error);
                }
              });
            } else {
              Meteor.call('photoNews', dataURI, str, scope, self._id._str, function (error, result) {
                if (!error) {
                  successCallback(result.newsId);
                } else {
                  // console.log('error',error);
                }
              });
            }
          };
          reader.readAsDataURL(file);
        }
      });
    }
  },
});

Template.newsAdd.onCreated(function () {
  const self = this;
  self.ready = new ReactiveVar();
  pageSession.set('error', false);

  self.autorun(function () {
    pageSession.set('scopeId', Router.current().params._id);
    pageSession.set('scope', Router.current().params.scope);
  });

  self.autorun(function () {
    const handle = Meteor.subscribe('scopeDetail', Router.current().params.scope, Router.current().params._id);
    if (handle.ready()) {
      self.ready.set(handle.ready());
    }
  });
});

Template.newsAdd.onRendered(function () {
  const self = this;
  pageSession.set('error', false);
  pageSession.set('queryMention', false);
  pageSession.set('queryTag', false);
  pageSession.set('mentions', false);
  pageSession.set('tags', false);
  self.$('textarea').atwho({
    at: '@',
    limit: 20,
    delay: 600,
    displayTimeout: 300,
    startWithSpace: true,
    displayTpl(item) {
      return item.avatar ? `<li><img src='${item.avatar}' height='20' width='20'/> ${item.name}</li>` : `<li>${item.name}</li>`;
    },
    insertTpl: '${atwho-at}${slug}',
    searchKey: 'name',
  }).atwho({
    at: '#',
  }).on('matched.atwho', function (event, flag, query) {
    // console.log(event, "matched " + flag + " and the result is " + query);
    if (flag === '@' && query) {
      // console.log(pageSession.get('queryMention'));
      if (pageSession.get('queryMention') !== query) {
        pageSession.set('queryMention', query);
        const querySearch = {};
        querySearch.search = query;
        Meteor.call('searchMemberautocomplete', querySearch, function (error, result) {
          if (!error) {
            const citoyensArray = _.map(result.citoyens, (array, key) => (array.profilThumbImageUrl ? {
              id: key, name: array.name, slug: (array.slug ? array.slug : array.name), type: 'citoyens', avatar: `${Meteor.settings.public.urlimage}${array.profilThumbImageUrl}`,
            } : {
              id: key, name: array.name, slug: (array.slug ? array.slug : array.name), type: 'citoyens',
            }));
            const organizationsArray = _.map(result.organizations, (array, key) => (array.profilThumbImageUrl ? {
              id: key, name: array.name, slug: (array.slug ? array.slug : array.name), type: 'organizations', avatar: `${Meteor.settings.public.urlimage}${array.profilThumbImageUrl}`,
            } : {
              id: key, name: array.name, slug: (array.slug ? array.slug : array.name), type: 'organizations',
            }));
            const arrayUnions = _.union(citoyensArray, organizationsArray);
            // console.log(citoyensArray);
            self.$('textarea').atwho('load', '@', arrayUnions).atwho('run');
          }
        });
      }
    } else if (flag === '#' && query) {
      // console.log(pageSession.get('queryTag'));
      if (pageSession.get('queryTag') !== query) {
        pageSession.set('queryTag', query);
        Meteor.call('searchTagautocomplete', query, function (error, result) {
          if (!error) {
            // console.log(result);
            self.$('textarea').atwho('load', '#', result).atwho('run');
          }
        });
      }
    }
  })
    .on('inserted.atwho', function (event, $li) {
      // console.log(JSON.stringify($li.data('item-data')));

      if ($li.data('item-data')['atwho-at'] === '@') {
        const mentions = {};
        // const arrayMentions = [];
        mentions.name = $li.data('item-data').name;
        mentions.id = $li.data('item-data').id;
        mentions.type = $li.data('item-data').type;
        mentions.avatar = $li.data('item-data').avatar;
        mentions.value = ($li.data('item-data').slug ? $li.data('item-data').slug : $li.data('item-data').name);
        mentions.slug = ($li.data('item-data').slug ? $li.data('item-data').slug : null);
        if (pageSession.get('mentions')) {
          const arrayMentions = pageSession.get('mentions');
          arrayMentions.push(mentions);
          pageSession.set('mentions', arrayMentions);
        } else {
          pageSession.set('mentions', [mentions]);
        }
      } else if ($li.data('item-data')['atwho-at'] === '#') {
        const tag = $li.data('item-data').name;
        if (pageSession.get('tags')) {
          const arrayTags = pageSession.get('tags');
          arrayTags.push(tag);
          pageSession.set('tags', arrayTags);
        } else {
          pageSession.set('tags', [tag]);
        }
      }
    });
});

Template.newsAdd.onDestroyed(function () {
  this.$('textarea').atwho('destroy');
});

Template.newsAdd.helpers({
  scope() {
    if (Router.current().params.scope) {
      const collection = nameToCollection(Router.current().params.scope);
      return collection.findOne({ _id: new Mongo.ObjectID(Router.current().params._id) });
    }
    return undefined;
  },
  error() {
    return pageSession.get('error');
  },
  blockSchema() {
    return Router.current().params.scope && SchemasNewsRestBase[Router.current().params.scope];
  },
  dataReady() {
    return Template.instance().ready.get();
  },
});

Template.newsEdit.onCreated(function () {
  const self = this;
  self.ready = new ReactiveVar();
  pageSession.set('error', false);

  self.autorun(function () {
    pageSession.set('scopeId', Router.current().params._id);
    pageSession.set('scope', Router.current().params.scope);
  });

  self.autorun(function () {
    const handle = Meteor.subscribe('scopeDetail', Router.current().params.scope, Router.current().params._id);
    const handleScopeDetail = Meteor.subscribe('newsDetail', Router.current().params.scope, Router.current().params._id, Router.current().params.newsId);
    if (handle.ready() && handleScopeDetail.ready()) {
      self.ready.set(handle.ready());
    }
  });
});

Template.newsEdit.onRendered(function () {
  pageSession.set('error', false);
  pageSession.set('queryMention', false);
  pageSession.set('queryTag', false);
  pageSession.set('mentions', false);
  pageSession.set('tags', false);
});

Template.newsFields.onRendered(function () {
  const self = this;
  self.$('textarea').atwho({
    at: '@',
    limit: 20,
    delay: 600,
    displayTimeout: 300,
    startWithSpace: true,
    displayTpl(item) {
      return item.avatar ? `<li><img src='${item.avatar}' height='20' width='20'/> ${item.name}</li>` : `<li>${item.name}</li>`;
    },
    insertTpl: '${atwho-at}${slug}',
    searchKey: 'name',
  }).atwho({
    at: '#',
  }).on('matched.atwho', function (event, flag, query) {
    // console.log(event, "matched " + flag + " and the result is " + query);
    if (flag === '@' && query) {
      // console.log(pageSession.get('queryMention'));
      if (pageSession.get('queryMention') !== query) {
        pageSession.set('queryMention', query);
        const querySearch = {};
        querySearch.search = query;
        Meteor.call('searchMemberautocomplete', querySearch, function (error, result) {
          if (!error) {
            const citoyensArray = _.map(result.citoyens, (array, key) => (array.profilThumbImageUrl ? {
              id: key, name: array.name, slug: (array.slug ? array.slug : array.name), type: 'citoyens', avatar: `${Meteor.settings.public.urlimage}${array.profilThumbImageUrl}`,
            } : {
              id: key, name: array.name, slug: (array.slug ? array.slug : array.name), type: 'citoyens',
            }));
            const organizationsArray = _.map(result.organizations, (array, key) => (array.profilThumbImageUrl ? {
              id: key, name: array.name, slug: (array.slug ? array.slug : array.name), type: 'organizations', avatar: `${Meteor.settings.public.urlimage}${array.profilThumbImageUrl}`,
            } : {
              id: key, name: array.name, slug: (array.slug ? array.slug : array.name), type: 'organizations',
            }));
            const arrayUnions = _.union(citoyensArray, organizationsArray);
            // console.log(citoyensArray);
            self.$('textarea').atwho('load', '@', arrayUnions).atwho('run');
          }
        });
      }
    } else if (flag === '#' && query) {
      // console.log(pageSession.get('queryTag'));
      if (pageSession.get('queryTag') !== query) {
        pageSession.set('queryTag', query);
        Meteor.call('searchTagautocomplete', query, function (error, result) {
          if (!error) {
            // console.log(result);
            self.$('textarea').atwho('load', '#', result).atwho('run');
          }
        });
      }
    }
  })
    .on('inserted.atwho', function (event, $li) {
      // console.log(JSON.stringify($li.data('item-data')));

      if ($li.data('item-data')['atwho-at'] === '@') {
        const mentions = {};
        // const arrayMentions = [];
        mentions.name = $li.data('item-data').name;
        mentions.id = $li.data('item-data').id;
        mentions.type = $li.data('item-data').type;
        mentions.avatar = $li.data('item-data').avatar;
        mentions.value = ($li.data('item-data').slug ? $li.data('item-data').slug : $li.data('item-data').name);
        mentions.slug = ($li.data('item-data').slug ? $li.data('item-data').slug : null);
        if (pageSession.get('mentions')) {
          const arrayMentions = pageSession.get('mentions');
          arrayMentions.push(mentions);
          pageSession.set('mentions', arrayMentions);
        } else {
          pageSession.set('mentions', [mentions]);
        }
      } else if ($li.data('item-data')['atwho-at'] === '#') {
        const tag = $li.data('item-data').name;
        if (pageSession.get('tags')) {
          const arrayTags = pageSession.get('tags');
          arrayTags.push(tag);
          pageSession.set('tags', arrayTags);
        } else {
          pageSession.set('tags', [tag]);
        }
      }
    });
});

Template.newsFields.onDestroyed(function () {
  this.$('textarea').atwho('destroy');
});

Template.newsEdit.helpers({
  new() {
    const news = News.findOne({ _id: new Mongo.ObjectID(Router.current().params.newsId) });
    const newEdit = {};
    newEdit._id = news._id._str;
    if (news && news.mentions) {
      pageSession.set('mentions', news.mentions);
    }
    if (news && news.tags) {
      pageSession.set('tags', news.tags);
    }
    newEdit.text = news.text;
    return newEdit;
  },
  blockSchema() {
    return Router.current().params.scope && SchemasNewsRestBase[Router.current().params.scope];
  },
  error() {
    return pageSession.get('error');
  },
  dataReady() {
    return Template.instance().ready.get();
  },
});

AutoForm.addHooks(['addNew', 'editNew'], {
  after: {
    method(error, result) {
      if (!error) {
        const selfresult = result.data.id.$id;
        const scopeId = pageSession.get('scopeId');
        const scope = pageSession.get('scope');

        const options = {
          width: 640,
          height: 480,
          quality: 75,
        };

        const successCallback = (retour) => {
          const newsId = retour;
          IonPopup.confirm({
            title: i18n.__('Photo'),
            template: i18n.__('Do you want to add another photo to this news'),
            onOk() {
              MeteorCameraUI.getPicture(options, function (errorCamera, data) {
                if (!errorCamera) {
                  const str = `${+new Date() + Math.floor((Math.random() * 100) + 1)}.jpg`;
                  Meteor.call('photoNews', data, str, scope, scopeId, newsId, function (errorPhoto, resultPhoto) {
                    if (!errorPhoto) {
                      successCallback(resultPhoto.newsId);
                    } else {
                      // console.log('error',error);
                    }
                  });
                }
              });
            },
            onCancel() {
              Router.go('newsList', { _id: pageSession.get('scopeId'), scope: pageSession.get('scope') });
            },
            cancelText: i18n.__('finish'),
            okText: i18n.__('other picture'),
          });
        };

        IonPopup.confirm({
          title: i18n.__('Photo'),
          template: i18n.__('Voulez vous prendre une photo ?'),
          onOk() {
            MeteorCameraUI.getPicture(options, function (errorCamera, data) {
              if (!errorCamera) {
                const str = `${+new Date() + Math.floor((Math.random() * 100) + 1)}.jpg`;
                Meteor.call('photoNews', data, str, scope, scopeId, selfresult, function (errorPhoto, photoret) {
                  if (!errorPhoto) {
                    successCallback(photoret.newsId);
                  } else {
                    // console.log('error', error);
                  }
                });
              }
            });
          },
          onCancel() {

          },
          cancelText: i18n.__('no'),
          okText: i18n.__('yes'),
        });

        // Meteor.call('pushNewNewsAttendees',scopeId,selfresult);
        Router.go('newsList', { _id: pageSession.get('scopeId'), scope: pageSession.get('scope') }, { replaceState: true });
      }
    },
    'method-update'(error) {
      if (!error) {
        Router.go('newsList', { _id: pageSession.get('scopeId'), scope: pageSession.get('scope') }, { replaceState: true });
      }
    },
  },
  before: {
    method(doc) {
      // console.log(doc);
      const scope = pageSession.get('scope');
      const scopeId = pageSession.get('scopeId');
      doc.parentType = scope;
      doc.parentId = scopeId;
      // comparer dans le text si @name present dans le array

      if (pageSession.get('mentions')) {
        const arrayMentions = Array.from(pageSession.get('mentions').reduce((m, t) => m.set(t.value, t), new Map()).values()).filter((array) => doc.text.match(`@${array.value}`) !== null);
        doc.mentions = arrayMentions;
      } else {
        // si on update est ce que la mention reste

      }
      const regex = /(?:^|\s)(?:#)([a-zA-Z\d]+)/gm;
      const matches = [];
      let match;
      while ((match = regex.exec(doc.text))) {
        matches.push(match[1]);
      }
      if (pageSession.get('tags')) {
        const arrayTags = _.reject(pageSession.get('tags'), (value) => doc.text.match(`#${value}`) === null, doc.text);
        if (doc.tags) {
          doc.tags = _.uniq(_.union(doc.tags, arrayTags, matches));
        } else {
          doc.tags = _.uniq(_.union(arrayTags, matches));
        }
      } else if (matches.length > 0) {
        if (doc.tags) {
          doc.tags = _.uniq(_.union(doc.tags, matches));
        } else {
          doc.tags = _.uniq(matches);
        }
      }
      return doc;
    },
    'method-update'(modifier) {
      const scope = pageSession.get('scope');
      const scopeId = pageSession.get('scopeId');
      modifier.$set.parentType = scope;
      modifier.$set.parentId = scopeId;
      if (pageSession.get('mentions')) {
        const arrayMentions = Array.from(pageSession.get('mentions').reduce((m, t) => m.set(t.value, t), new Map()).values()).filter((array) => modifier.$set.text.match(`@${array.value}`) !== null);
        modifier.$set.mentions = arrayMentions;
      } else {
        // si on update est ce que la mention reste

      }

      const regex = /(?:^|\s)(?:#)([a-zA-Z\d]+)/gm;
      const matches = [];
      let match;
      while ((match = regex.exec(modifier.$set.text))) {
        matches.push(match[1]);
      }
      if (pageSession.get('tags')) {
        const arrayTags = _.reject(pageSession.get('tags'), (value) => modifier.$set.text.match(`#${value}`) === null, modifier.$set.text);
        if (modifier.$set.tags) {
          modifier.$set.tags = _.uniq(_.union(modifier.$set.tags, arrayTags, matches));
        } else {
          modifier.$set.tags = _.uniq(_.union(arrayTags, matches));
        }
      } else if (matches.length > 0) {
        if (modifier.$set) {
          modifier.$set = _.uniq(_.union(modifier.$set, matches));
        } else {
          modifier.$set = _.uniq(matches);
        }
      }
      return modifier;
    },
  },
  onError(formType, error) {
    if (error.errorType && error.errorType === 'Meteor.Error') {
      if (error && error.error === 'error_call') {
        pageSession.set('error', error.reason.replace(':', ' '));
        IonToast.show({
          title: i18n.__('Error'), position: 'top', type: 'error', showClose: true, template: `<i class="icon fa fa-exclamation-circle"></i> ${error && error.reason ? error.reason.replace(': ', '') : error.error}`,
        });
      }
    }
  },
});

Template._inviteattendeesEvent.onCreated(function () {
  pageSession.set('error', false);
  pageSession.set('invitedUserEmail', false);
});

Template._inviteattendeesEvent.onRendered(function () {
  pageSession.set('error', false);
  pageSession.set('invitedUserEmail', false);
});

Template._inviteattendeesEvent.helpers({
  error() {
    return pageSession.get('error');
  },
});

AutoForm.addHooks(['inviteAttendeesEvent'], {
  before: {
    method(doc) {
      const scopeId = pageSession.get('scopeId');
      doc.eventId = scopeId;
      pageSession.set('invitedUserEmail', doc.invitedUserEmail);
      return doc;
    },
  },
  after: {
    method(error) {
      if (!error) {
        IonModal.close();
      }
    },
  },
  onError(formType, error) {
    // console.log(error);
    if (error.errorType && error.errorType === 'Meteor.Error') {
      if (error && error.error === 'error_call') {
        if (error.reason === "Problème à l'insertion du nouvel utilisateur : une personne avec cet mail existe déjà sur la plateforme") {
          Meteor.call('saveattendeesEvent', pageSession.get('scopeId'), pageSession.get('invitedUserEmail'), function (errorSave) {
            if (errorSave) {
              pageSession.set('error', error.reason.replace(':', ' '));
              IonToast.show({
                title: i18n.__('Error'), position: 'top', type: 'error', showClose: true, template: `<i class="icon fa fa-exclamation-circle"></i> ${error && error.reason ? error.reason.replace(': ', '') : error.error}`,
              });
            } else {
              IonModal.close();
            }
          });
        }
      } else {
        pageSession.set('error', error.reason.replace(':', ' '));
        IonToast.show({
          title: i18n.__('Error'), position: 'top', type: 'error', showClose: true, template: `<i class="icon fa fa-exclamation-circle"></i> ${error && error.reason ? error.reason.replace(': ', '') : error.error}`,
        });
      }
    }
  },
});
