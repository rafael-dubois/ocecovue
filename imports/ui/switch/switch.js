import { Meteor } from 'meteor/meteor';
import { Template } from 'meteor/templating';
import { ReactiveVar } from 'meteor/reactive-var';
import { Mongo } from 'meteor/mongo';

// collection
import { Organizations } from '../../api/collection/organizations.js';
import { Citoyens } from '../../api/collection/citoyens.js';

import './switch.html';

window.Organizations = Organizations;

Template.switch.onCreated(function () {
  this.ready = new ReactiveVar(false);
  this.autorun(function () {
    const handle = this.subscribe('orga.switch');
    this.ready.set(handle.ready());
  }.bind(this));
});

Template.switch.helpers({
  OrganizationsOceco() {
    return Organizations.find({ oceco: { $exists: true }, parent: { $exists: false } });
  },
  citoyenScope() {
    return Citoyens.findOne({ _id: new Mongo.ObjectID(Meteor.userId()) }, { fields: { pwd: 0 } });
  },
  dataReady() {
    return Template.instance().ready.get();
  },
});
