import { Template } from 'meteor/templating';
import { lazy } from '../components/iolazyload.js';

import './images.html';

Template.imgDoc.onRendered(function () {
  // eslint-disable-next-line no-new
  this.autorun(function () {
    if (Template.currentData().profilThumbImageUrl) {
      lazy.lazyLoad();
    }
  });
});
